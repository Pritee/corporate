<?php
class Welcome extends CI_Controller {
function __construct() {
parent::__construct();
$this->load->library(array('session'));
$this->load->helper(array('form', 'url'));
$this->load->library(array('form_validation'));
$this->load->model('CI_auth');
$this->load->database();

}

function index()
{

    $this->CI_auth->getMessage();
    $this->CI_auth->getWorkList();
    $this->CI_auth->getTeamList();
    $this->CI_auth->readIdea();

  if ($_SESSION['user_position']=='Manager') {
            $this->load->view('manager');
         }
         elseif ($_SESSION['user_position']=='TeamLeader') {	
                    	 $this->load->view('teamleader');
                    }
                    elseif ($_SESSION['user_position']=='Employee') {
	                                $this->load->view('employee_page');
                                }
                               elseif ($_SESSION['user_position']=='Admin') {
                                            $this->load->view('admin_page');
                                          }
                                        
}

function registerEmployee(){
         $this->load->view('register_employee');
         
       }

function viewIdeaGraph(){


        $data['idea_graph'] =$this->CI_auth->getIdeasGraph();
         $this->load->view('idea_graph',$data);
       }
function viewProjectGraph(){


        $data['project_graph'] =$this->CI_auth->getProjectGraph();
         $this->load->view('project_graph',$data);
       }


function employeeInformation(){
         $this->load->view('get_employee_info');
       }
function ideaInformation(){
         $this->load->view('get_ideas');
       }

/*function viewideasInfo(){
        $data['ideas'] =$this->CI_auth->getIdeas();
         $this->load->view('get_ideas',$data);
       }
*/

function addNewEmployee(){
  $rand_salt = $this->CI_encrypt->genRndSalt();

if($this->input->post('register')==true) {

$input_data = array(
'unique_id' => $this->input->post('u_id'),
'name' => $this->input->post('name'),
'username' => $this->input->post('username'),
'date_of_birth' =>$this->input->post('dob'),
'reputation'=>$this->input->post('reputation'),
'hired_date'=>$this->input->post('hired_date'),
'team_leader_id' => $this->input->post('t_id'),
'manager_id' => $this->input->post('m_id'),
'salary'=>$this->input->post('salary'),
'email' => $this->input->post('email'),
'phone' => $this->input->post('phone'),
'password' => $this->CI_encrypt->encryptUserPwd( $this->input->post('password'),$rand_salt),
'salt'=>$rand_salt,
'position' => $this->input->post('position'),
'address' => $this->input->post('address'),
'gender' => $this->input->post('gender'),
'terms' => $this->input->post('terms')
);
$result = $this->CI_auth->insertEmployee($input_data);
}
echo " inserted Successfully!";
 redirect("Welcome/index");
}

function employeeInfoDetails(){
          $position1 = $this->input->post('multiple');
          $data['info'] = $this->CI_auth->getEmpInfo($position1);
           $this->load->view('employee_info.php',$data);


}
function ideaInfoDetails(){
          $status = $this->input->post('multiple');
          $data['info1'] = $this->CI_auth->getIdeaInfo($status);
           $this->load->view('idea_info.php',$data);
}



function aboutCompany(){
         $this->load->view('about_company');
       }


function sentMail(){
          $this->load->view('compose');
        }


function currentProjects(){
          $this->load->view('current_projects');
        }


function pendingProjects(){
          $this->load->view('pending_projects');
        }


function futureProjects(){  
          $this->load->view('future_projects');
        }


function ideasList(){
          $this->load->view('idealist.php');
        }


function detailProject($project_id){
          $data['p_id']=$project_id;
          $this->load->view('detail_project.php',$data);
        }


function timeline(){
          $this->load->view('timeline.php');
        }


function profile(){
          $this->load->model('CI_auth');
          $data= $this->CI_auth->profile();
          $this->load->view('profile' , $data);
        }


function updateProfile(){
          $user_id= $this->session->userdata('user_id');
          $name = $this->input->post('name');
          $email = $this->input->post('email');
          $phone = $this->input->post('phone');
          $dob = $this->input->post('dob');
          $this->load->model('CI_encrypt');
          $rand_salt = $this->CI_encrypt->genRndSalt();
          $password = $this->CI_encrypt->encryptUserPwd( $this->input->post('password'),$rand_salt);        
         
          // table column name should be same as data object key name
          $result = $this->CI_auth->updateProfile($name,$email,$phone
          ,$dob,$password,$rand_salt,$user_id); // call the employee model
          if($result==true){
             // echo "Update inserted Successfully!";
            $this->load->view('employee_page');
          }
            else {
              $this->load->view('employee_page');
              echo "Update didn't inserted Successfully!";
            }
        }


function calendar(){
          $this->load->view('calendar');
        }

function subscribe()
{
  if($this->input->post('email_sub')==true){
    $email= array('email' => $this->input->post('email'), );
    $result = $this->CI_auth->insertEmail($email);
  }
  redirect("Welcome/aboutCompany");

}
function shareIdea(){
          if ($this->input->post('idea')==true){
                  // create data
		              $idea = array(
				          'project_id' => $this->input->post('project_id'),
				          'to_whom' => $this->input->post('to_whom'),
				          'subject' => $this->input->post('subject'),
                  'code'=> $this->input->post('code'),
                  'idea_date'=> date('Y-m-d'),
                  'status'=>'send',
                  'ID'=>$this->session->userdata('user_id')
                  );
		           // table column name should be same as data object key name
		          $result = $this->CI_auth->insertIdea($idea); // call the employee model
            }
          redirect("Welcome/index");
        }


function readIdea(){
          $this->CI_auth->readIdea();
        }


function viewIdea($idea_id){
          $data['idea_data']= $this->CI_auth->viewIdea($idea_id);
          $this->load->view('view_idea',$data);
        }


function acceptIdea($idea_id){
          $this->CI_auth->ideaAcceptResult($idea_id);
          redirect("Welcome/index");
        }


function rejectIdea($idea_id){
          $this->CI_auth->ideaRejectResult($idea_id);
          redirect("Welcome/index");
        }


function newLink(){
          $receiver_id = $this->input->post('receiver_id');
          $message = $this->input->post('message');
          $this->CI_auth->createNewMessagelink($receiver_id,$message);
          $chat_id=($this->session->userdata('chat_id'));
          redirect("Welcome/chat/$chat_id/$receiver_id");
        }


function newLink1($receiver_id){
          $message = $this->input->post('message');
          $this->CI_auth->createNewMessagelink($receiver_id,$message);
          $chat_id=($this->session->userdata('chat_id'));
          redirect("Welcome/chat/$chat_id/$receiver_id");
        }


function chat($chat_id,$sender_id){
          $data['chats'] = $this->CI_auth->chatting($chat_id,$sender_id);
          $this->load->view('view_chat',$data);
        }


function sendMail(){
          $to = $this->input->post('to');
          $subject = $this->input->post('subject');
          $message = $this->input->post('message');

          $config['protocol'] = 'smtp';
          $config['smtp_host'] = 'ssl://smtp.googlemail.com';
          $config['smtp_port'] = 465;
          $config['smtp_user'] = 'corporateworld2017@gmail.com';
          $config['smtp_pass'] = 'ganzore12';

          $this->load->library('email', $config);
          $this->email->set_newline("\r\n");
            
          $html= "Test email";
          $email_setting = array('mailtype'=>'html');
          $this->email->initialize($email_setting);
          $this->email->from('corporateworld2017@gmail.com');
          $this->email->to($to);
          $this->email->subject($subject);
          $this->email->message($message);

            if ($this->email->send()) {
                         redirect("Welcome/index");
                    }
                     else {
                            show_error($this->email->print_debugger());
                          }
   }




function work($work_id){
          $this->CI_auth->work($work_id);
          $this->load->view('work_list');
        }


function assignProjectWork(){
          $data = ($this->session->userdata['title']);
          $title= $data['title'];
              foreach ($title as $key => $value) {
                              $project_id=$value->project_id;
                              $base_work_id=$value ->work_id;
                            }
          $title = $this->input->post('title');
          $descr = $this->input->post('descr');

                if ($this->session->userdata('user_position')=='Employee') {
                              $receiver = $this->session->userdata('user_id');
                            }
                            else {
                                    $receiver = $this->input->post('receiver');
                                  }
          $days = $this->input->post('days');
          $this->CI_auth->assignProjectWork($project_id,$base_work_id,$receiver,$title,$descr,$days);

          redirect("Welcome/index");
        }



/*function assignNewWork(){
          $title = $this->input->post('title');
          $descr = $this->input->post('descr');
          $days = $this->input->post('days');
          $receiver = $this->input->post('receiver');

          $this->CI_auth->assignNewWork($title,$descr,$days,$receiver);
 
          redirect("Welcome/index");
        }
*/

function submitWork(){
              foreach ($_POST['work_id'] as $work_id) {
                              $this->CI_auth->submitWork($work_id);
                            }
  
          return redirect('welcome/index');
        }


function submitAdminWork(){
              foreach ($_POST['work_id'] as $work_id) {
                              $this->CI_auth->submitAdminWork($work_id);
                            }
  
          return redirect('welcome/index');
        }



function markAsCompleteWork(){
              foreach ($_POST['work_id'] as $work_id) {
                              $this->CI_auth->markAsCompleteWork($work_id);
                            }

          return redirect('welcome/index');
        }


function increments($unique_id){
          $data['increment_id'] = $unique_id;
          $this->load->view('salary_incr',$data);
        }


function salaryIncrement($unique_id){
          $salary=$this->input->post('salary');
          $position=$this->input->post('position');

          $this->CI_auth->salaryIncrement($unique_id,$salary,$position);
          return redirect('welcome/index');
        }

function uploadFile(){
 
    $data = array();
    if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
      $filesCount = count($_FILES['userFiles']['name']);
      for($i = 0; $i < $filesCount; $i++){
        $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
        $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
        $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
        $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
        $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

        $uploadPath = 'uploads/files/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = '*';
        //$config['max_size'] = '100';
        //$config['max_width'] = '1024';
        //$config['max_height'] = '768';
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('userFile')){
          $fileData = $this->upload->data();
          $uploadData[$i]['file_name'] = $fileData['file_name'];
          $uploadData[$i]['created'] = date("Y-m-d H:i:s");
          $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
        }
      }
      if(!empty($uploadData)){
        //Insert files data into the database
        $statusMsg = $insert?'Files uploaded successfully.':'Some problem occurred, please try again.';
        echo "<script type='text/javascript'>alert('$statusMsg');</script>";
        $this->session->set_flashdata('statusMsg',$statusMsg);
      }
      
    }
    //get files data from database
         $title = $this->input->post('title');
          $descr = $this->input->post('descr');
          $days = $this->input->post('days');
          $receiver = $this->input->post('receiver');
          $file_name = $fileData['file_name'];

          $this->CI_auth->assignNewWork($title,$descr,$days,$receiver,$file_name);
    //pass the files data to view
          return redirect('welcome/index');
  }

function uploadFile2(){
 
    $data = array();
    if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
      $filesCount = count($_FILES['userFiles']['name']);
      for($i = 0; $i < $filesCount; $i++){
        $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
        $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
        $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
        $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
        $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

        $uploadPath = 'uploads/files/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = '*';
        //$config['max_size'] = '100';
        //$config['max_width'] = '1024';
        //$config['max_height'] = '768';
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('userFile')){
          $fileData = $this->upload->data();
          $uploadData[$i]['file_name'] = $fileData['file_name'];
          $uploadData[$i]['created'] = date("Y-m-d H:i:s");
          $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
        }
      }
      if(!empty($uploadData)){
        //Insert files data into the database
        $statusMsg = $insert?'Files uploaded successfully.':'Some problem occurred, please try again.';
        echo "<script type='text/javascript'>alert('$statusMsg');</script>";
        $this->session->set_flashdata('statusMsg',$statusMsg);
      }
      
    }
    //get files data from database
         $title = $this->input->post('title');
          $descr = $this->input->post('descr');
          $days = $this->input->post('days');
          $receiver = $this->input->post('receiver');
           $type = $this->input->post('projectType');
          $file_name = $fileData['file_name'];

          $this->CI_auth->assignNewProject($title,$descr,$days,$receiver,$file_name,$type);
    //pass the files data to view
          return redirect('welcome/index');
  }



function uploadFile1(){
 
    $data = array();
    if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
      $filesCount = count($_FILES['userFiles']['name']);
      for($i = 0; $i < $filesCount; $i++){
        $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
        $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
        $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
        $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
        $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

        $uploadPath = 'uploads/files/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = '*';
        //$config['max_size'] = '100';
        //$config['max_width'] = '1024';
        //$config['max_height'] = '768';
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('userFile')){
          $fileData = $this->upload->data();
          $uploadData[$i]['file_name'] = $fileData['file_name'];
          $uploadData[$i]['created'] = date("Y-m-d H:i:s");
          $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
        }
      }
      if(!empty($uploadData)){
        //Insert files data into the database
        $statusMsg = $insert?'Files uploaded successfully.':'Some problem occurred, please try again.';
        echo "<script type='text/javascript'>alert('$statusMsg');</script>";
        $this->session->set_flashdata('statusMsg',$statusMsg);
      }
      
    }
    //get files data from database
         $title = $this->input->post('title');
          $descr = $this->input->post('descr');
          $days = $this->input->post('days');
          $file_name = $fileData['file_name'];
          $this->CI_auth->addNewWork($title,$descr,$days,$file_name);
    //pass the files data to view
          return redirect('welcome/index');
  }


function addNewAdminWork(){

   $title = $this->input->post('title');
          $this->CI_auth->addNewAdminWork($title);

          return redirect('welcome/index');
  }

function download($file_name){
                  if(!empty($file_name)){
                          //load download helper
                          $this->load->helper('download'); 
                          //get file info from database
                          $fileInfo = file_get_contents("./uploads/files/.'".$file_name."'");
                          //file path
                          $name = $file_name;
                          //download file from directory
                          force_download($name, $fileInfo);
                        }
        }

 
 /*Get all Events */

  Public function getEvents()
  {
    $result=$this->CI_auth->getEvents();
    echo json_encode($result);
  }
  /*Add new event */
  Public function addEvent()
  {
    $result=$this->CI_auth->addEvent();
    echo $result;
  }
  /*Update Event */
  Public function updateEvent()
  {
    $result=$this->CI_auth->updateEvent();
    echo $result;
  }
  /*Delete Event*/
  Public function deleteEvent()
  {
    $result=$this->CI_auth->deleteEvent();
    echo $result;
  }
  Public function dragUpdateEvent()
  { 

    $result=$this->CI_auth->dragUpdateEvent();
    echo $result;
  }       
}
/* End of file welcome.php */

/* Location: ./system/application/controllers/welcome.php */
