<?php
class Register extends CI_Controller {
function __construct()
{
parent::__construct();
$this->load->library(array('form_validation'));
$this->load->model(array('CI_captcha','CI_auth','CI_encrypt'));
$this->load->helper(array('form', 'url'));
$this->load->database();
}

function index(){
if($this->CI_auth->check_logged()=== true)
redirect(base_url().'index.php/welcome/index/');

$data['title'] = 'CodeIgniter Registration System';
$sub_data['captcha_return'] ='';
$sub_data['cap_img'] = $this ->CI_captcha->make_captcha();
if($this->input->post('submit')) {
$this->form_validation->set_rules('u_id', 'Unique_Id');
$this->form_validation->set_rules('name', 'Name');
$this->form_validation->set_rules('username', 'User name');
$this->form_validation->set_rules('dob', 'DOB');
$this->form_validation->set_rules('password', 'Password');
$this->form_validation->set_rules('passconf', 'Confirm Password');
$this->form_validation->set_rules('email', 'Email');
$this->form_validation->set_rules('phone', 'Mobile_No');
$this->form_validation->set_rules('position', 'position');
$this->form_validation->set_rules('address', 'Address');
$this->form_validation->set_rules('gender', 'Gender');
$this->form_validation->set_rules('terms', 'Terms of Sevices');
$this->form_validation->set_rules('captcha', 'Captcha', 'required');

// Set Custom messages
//$this->form_validation->set_message('required', 'Your custom message here');


if ($this->form_validation->run() == FALSE){
$data['body']  = $this->load->view('_join_form', $sub_data, true);
}
else{
if($this->CI_captcha->check_captcha()==TRUE){
$u_id = $this->input->post('u_id');
$name = $this->input->post('name');
$username = $this->input->post('username');
$dob = $this->input->post('dob');
$password = $this->input->post('password');
$email = $this->input->post('email');
$phone = $this->input->post('phone');
$position = $this->input->post('position');
$address = $this->input->post('address');
$gender = $this->input->post('gender');
$terms = $this->input->post('terms');
$check_query = "SELECT * FROM `users1` WHERE `username`='$username' OR `email`='$email'";
$query = $this->db->query($check_query);
if ($query->num_rows() > 0){
$sub_data['captcha_return'] = 'username or email address you entered is already used by another, please change<br/>';
$data['body']  = $this->load->view('_join_form', $sub_data, true);
}
else{
$rand_salt = $this->CI_encrypt->genRndSalt();
$encrypt_pass = $this->CI_encrypt->encryptUserPwd( $this->input->post('password'),$rand_salt);
$input_data = array(
'unique_id'	=> $u_id,
'name' => $name,
'username' => $username,
'date_of_birth'	=>$dob,
'email' => $email,
'phone'	=> $phone,
'password' => $encrypt_pass,
'position' => $position,
'address' => $address,
'gender' => $gender,
'salt' => $rand_salt
);
if($this->db->insert('users1', $input_data)){
$data['body']  = "Registration success, please login<br/>";
redirect(base_url().'index.php/login/');
}
else
$data['body']  = "error on query";
}
}
else{
$sub_data['captcha_return'] = "The characters you entered didn't match the word verification. Please try again. <br/>";
$data['body']  = $this->load->view('_join_form', $sub_data, true);
}
}

}
else{
$data['body']  = $this->load->view('_join_form', $sub_data, true);
}
$this->load->view('_output_html', $data);

}
}
?>