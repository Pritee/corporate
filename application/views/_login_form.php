<!DOCTYPE html>
<html>
<head>
<title>MY Organization</title>
</head>

<link href='<?php echo base_url(). "assets/css/style1.css" ?>' rel="stylesheet" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</script>
<!--webfonts-->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<script>var __links = document.querySelectorAll('a');function __linkClick(e) { parent.window.postMessage(this.href, '*');} ;for (var i = 0, l = __links.length; i < l; i++) {if ( __links[i].getAttribute('data-t') == '_blank' ) { __links[i].addEventListener('click', __linkClick, false);}}</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>$(document).ready(function(c) {
	$('.alert-close').on('click', function(c){
		$('.message').fadeOut('slow', function(c){
	  		$('.message').remove();
		});
	});	  
});
</script>

<div class="shadow-forms">
	<div class="message warning">
		<div class="login-head">
			 <div class="alert-close"> </div>
			 <?php echo form_open(base_url().'index.php/login/')?>
			 <h2>Client Login</h2>
		</div>

		<!--<div class="sub-head">
			<h3>Your almost done ! One more step</h3>
			<img src='<?php echo base_url(). "assets/images/bbl.png" ?>' alt=""/>
		</div>-->
		<br>
		<form>
			<?php echo validation_errors(); ?>
			 <?php echo $login_failed; ?>
			<input  type="text" class="text"  name="username" placeholder="Username" value="<?php echo set_value('username'); ?>">

			<input type="password" name="password" placeholder="Password" value="<?php echo set_value('password'); ?>">
			
			<div class="submit">
				<input type="submit" onclick="myFunction()" value="LOG IN"  name="submit_login">
			</div>
			<div class="p-container">
				<input  type="checkbox" name="checkbox" checked><i></i>Remember Me
				
				<h6><a href="#" style="margin-top:-20px;">Forgot a Password ?&nbsp;<br>Contact Admin</a></h6>
				<br><br>
				<div class="clear"> </div>
			</div>
			<?php echo form_close()?>		
		</form>
				
				<div class="sign-up">
					<!-- pop-up-box -->
				<script type="text/javascript" src='<?php echo base_url(). "assets/js/modernizr.custom.min.js" ?>'></script>    
				<link href='<?php echo base_url()."assets/css/popuo-box.css" ?>' rel="stylesheet" type="text/css" media="all" />
				<script src='<?php echo base_url(). "assets/js/jquery.magnific-popup.js" ?>' type="text/javascript"></script>
				
			<script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>
		</div>

					<div class="clear"> </div>
				</div>
			
		</div>
	</div>
<!--- footer -->
<div class="footer">
	<p>Copyright &copy; 2017</a></p>

</div>
