<!DOCTYPE html>
<html>
<head>
<title>MY Organization</title>
</head>
<link href='<?php echo base_url(). "assets/css/style1.css" ?>' rel="stylesheet" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</script>
<!--webfonts-->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<script>var __links = document.querySelectorAll('a');function __linkClick(e) { parent.window.postMessage(this.href, '*');} ;for (var i = 0, l = __links.length; i < l; i++) {if ( __links[i].getAttribute('data-t') == '_blank' ) { __links[i].addEventListener('click', __linkClick, false);}}</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>$(document).ready(function(c) {
	$('.alert-close').on('click', function(c){
		$('.message').fadeOut('slow', function(c){
	  		$('.message').remove();
		});
	});	  
});
</script>

<div class="shadow-forms">
	<div class="message warning">
		<div class="login-head">
			 <div class="alert-close"> </div>
			 <?php echo form_open(base_url()."index.php/register/")?>
			 <h2>Registration Form</h2>
		</div>

		<!--<div class="sub-head">
			<h3>Your almost done ! One more step</h3>
			<img src='<?php echo base_url(). "assets/images/bbl.png" ?>' alt=""/>
		</div>-->
		<br>
		<form>
			<?php echo validation_errors() ?>
			<?php echo $captcha_return?>

			<input type="text" class="text" name="u_id" value="<?php echo set_value('Unique_Id') ?>" placeholder="Unique_Id" required pattern="\w+"/>

			<input type="text" class="text" name="name" value="<?php echo set_value('name') ?>" placeholder="Your Name" required pattern="\w+"/>

			<input type="text" class="text" name="username" value="<?php echo set_value('username') ?>" placeholder="Username" required pattern="\w+"/>

			<input type="password" name="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" onchange="form.passconf.pattern = this.value; value="<?php echo set_value('password') ?>" placeholder="Password" />

			<input type="password" name="passconf" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" value="<?php echo set_value('passconf') ?>" placeholder="Confirm Password" />

			<input type="text" name="email" value="<?php echo set_value('email') ?>" placeholder="Email ID"/>

			<input type="text" name="phone" class="text" value="<?php echo set_value('phone') ?>" placeholder="Mobile No" maxlength="10" required/>

			<textarea class="textarea" type="area" name="address" placeholder="Your Address" required pattern="\w+" value="<?php echo set_value('address') ?>"></textarea>
			
			<select name="position" type="text" required pattern="\w+">
			<option value="" class="text">Select Position</option>
			<option value="Admin" <?php echo set_select('position', 'Admin'); ?>>Admin</option>
			<option value="CEO" <?php echo set_select('position', 'CEO'); ?> >CEO</option>
			<!--<option value="ManagingDirector" <?php echo set_select('position', 'Managing Director'); ?> >Managing Director</option>-->
			<option value="Manager" <?php echo set_select('position', 'Manager'); ?> >Manager</option>
			<option value="TeamLeader" <?php echo set_select('position', 'Team Leader'); ?> >Team Leader</option>
			<option value="Employee" <?php echo set_select('position', 'Employee'); ?> >Employee</option>
			</select><br>
		
			<input type="date" class="text" name="dob" value="<?php echo set_value('dob') ?>" placeholder="DOB" required/>

			<input type="radio" name="gender" value="Female"  <?php echo set_radio('gender', 'Male'); ?>/> Male &nbsp;&nbsp;   
			<input type="radio" name="gender" value="Male"  <?php echo set_radio('gender', 'Female'); ?>/>Female 
			<br>
			
			<p style="margin-left: 30px;">Type the Captcha number below:
			<?php echo $cap_img; ?>
			<input type="text" name="captcha" value=""/>
			<br>
			<input type="checkbox" name="terms" value="1" <?php echo set_checkbox('terms', '1'); ?> />I agree to the Terms of Service and Privacy Policy
			
			<div class="submit">
				<input class=" " type="submit" value="SIGN UP" name="submit"/>
			</div>
			<br>
			<?php echo form_close()?>		
		</form>

				<div class="sign-up">
					<a href='<?php echo base_url().'index.php/login/'?>' class="sign-left">Already have an  account ?</a>
					<a  class="signup play-icon popup-with-zoom-anim" href='<?php echo base_url().'index.php/login/'?>'>Sign In</a>

					<!-- pop-up-box -->
				<script type="text/javascript" src='<?php echo base_url(). "assets/js/modernizr.custom.min.js" ?>'></script>    
				<link href='<?php echo base_url()."assets/css/popuo-box.css" ?>' rel="stylesheet" type="text/css" media="all" />
				<script src='<?php echo base_url(). "assets/js/jquery.magnific-popup.js" ?>' type="text/javascript"></script>
				
			<script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>
		</div>

					<div class="clear"> </div>
				</div>
			
		</div>
	</div>
<!--- footer -->
<div class="footer">
	<p>Copyright &copy; 2017</a></p>

</div>
</body>
</html>