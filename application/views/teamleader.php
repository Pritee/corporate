<?php include 'header.php';




$data = ($this->session->userdata['work_list']);
$work_list= $data['work_list'];




$data = ($this->session->userdata['track_list']);
$track_list= $data['track_list'];



$data = ($this->session->userdata['progress']);
$progress= $data['progress'];


?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!--<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>-->

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
      <div class="col-md-12">
          <div class="box box-solid">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="http://placehold.it/1250x200/39CCCC/ffffff&text=Be A TeamLeader!" alt="First slide">
                    <br>
                    <div class="carousel-caption">
                       <h3 style="color: black">A team is a reflection of its leadership.</h3>
                    </div>
                  </div>
                  <div class="item">
                    <img src="http://placehold.it/1250x200/3c8dbc/ffffff&text=True leaders don't create followers..." alt="Second slide">
                    <br>
                    <div class="carousel-caption">
                       <h3 style="color: black">they create more leaders!</h3>
                    </div>
                  </div>
                  <div class="item">
                    <img src="http://placehold.it/1250x200/f39c12/ffffff&text=A leader must inspire or" alt="Third slide">
                    <br>
                    <div class="carousel-caption">
                       <h3 style="color: black">his/her team will expire!</h3>
                    </div>    
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

              <div class="box col-md-12">
            <div class="box-header with-border">
              <h3 class="box-title">Track Employees' work</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>TeamLeader Name</th>
                  <th>Work Assigned</th>
                  <th>Progress</th>
                  <th style="width: 40px">Label</th>
                  <th>Days Remaining</th>
                </tr>
                <?php 

                 $track_count = 0 ;
                if (empty($track_list)) {
                  echo 'empty';
                  $empty=0;
                }
                else{
                    $finalPercentage=0;
                    $count1 =0;
                  $empty=1;
                  foreach ($track_list as $key => $value) { ?>
                 <tr>
                  <td><?php echo $track_count= $track_count + 1; ?>.</td>
                  <td><?php echo $value->name; ?></td>
                  <td><?php echo $value->work_title; ?></td>
                  <td>
                    <div class="progress progress-xs">
                    <?php 
                      $totalDays = 0;
                      $days = 0;
                    foreach ($progress as $key) {
                      if ($key->base_work_id==$value->work_id) {

                              $totalDays=$totalDays + $key->days;
                              if ($key->work_status=='completed') {

                                $days=$days + $key->days;
                              }
                      }
                    } 

                    if ($days == 0) {

                       $percentage = 0;
                    }
                    else{
                        $percentage = $days/$totalDays * 100;
                    }
                     
                      $finalPercentage = $finalPercentage + $percentage;
                       $count1=$count1 + 1;


                    ?><?php
                    if ($percentage >= 75 ) {?>
                      <div class="progress-bar progress-bar-success" style="width: <?php echo $percentage; ?>%"></div>
                      <?php
                       }
                      else{
                          if ($percentage >= 50 ) {?>
                                   <div class="progress-bar progress-bar-yellow" style="width: <?php echo $percentage; ?>%"></div>
                            <?php  }
                            else {
                                 if ($percentage >= 25) { ?>
                                           <div class="progress-bar progress-bar-primary" style="width: <?php echo $percentage; ?>%"></div>
                                  <?php  }
                                  else{
                                    if ($percentage <= 25) {?>
                                             <div class="progress-bar progress-bar-primary" style="width: <?php echo $percentage; ?>%"></div>
                                    <?php  }
                                  }
                               }
                          }
                     ?>
                    </div>
                  </td>
                  <td><span class="badge bg-gray"><?php echo intval($percentage); ?>%</span></td>
                  <td><?php 
                      $now = time(); // or your date as well
                      $your_date = strtotime($value->assign_date);
                      $datediff = $now - $your_date;

                       $no_days_gone = floor($datediff / (60 * 60 * 24));
                       $no_days_remaining = $value->days - $no_days_gone;
                       echo $no_days_remaining;
                   ?>/<?php echo $value->days ?></td>
                </tr>
                <?php }
              $count1=$count1*100;  }?>
               
             </table>
            </div>
          </div>
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          

          

          <!-- TO DO List -->
           <form role="form"  method="post" action="<?php echo base_url('/index.php/Welcome/submitWork');?>">
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">To Do List</h3>

              <div class="box-tools pull-right">
                <ul class="pagination pagination-sm inline">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="todo-list">
                <?php  $work_count = 0 ;
                if (empty($work_list)) {
                  echo 'empty';
                  # code...
                }
                else{

                  $count=0;
             foreach ($work_list as $key => $value) { 
                        $work_id = $value->work_id;
                        ?>
                                  <li>     
                  <!-- checkbox -->
                  <input type="checkbox" value="<?php echo $value->work_id; ?>" name="work_id[]";>

                   
                  <!-- todo text -->

                  <?php

                  foreach ($work_notification as $key => $val) {
                          $work_notification_id =$val->work_id; 
                               if($work_id == $work_notification_id){ 
                                $count=1;
                   ?>
                   <span class="" style="color:green;"><?php echo $work_count= $work_count + 1; ?>.
                  </span>
                  <span class="text"><a  style="cursor:pointer ; color:green;" href="<?php echo base_url() ?>index.php/Welcome/work/<?php echo $value->work_id ?>"><?php echo $value->work_title ?></a></span>

                  <?php

                   }
                }

                   if($count!=1){
                    ?>
                    <span class=""><?php echo $work_count= $work_count + 1; ?>.
                  </span>
                  <span class="text"><a  style="cursor:pointer" href="<?php echo base_url() ?>index.php/Welcome/work/<?php echo $value->work_id ?>"><?php echo $value->work_title ?></a></span>

                <?php
                  }
                            $count=0;
                   ?>

                  <!-- Emphasis label -->
                   <?php 
                      $now = time(); // or your date as well
                      $your_date = strtotime($value->assign_date);
                      $datediff = $now - $your_date;

                       $no_days_gone = floor($datediff / (60 * 60 * 24));
                       $no_days_remaining = $value->days - $no_days_gone;
                  if ($no_days_remaining >=10) {?>
                   <small class="label label-primary"><i class="fa fa-clock-o"></i><?php echo ' ',$no_days_remaining ,' days remaining';?>
                  </small>
                   <?php  
                 } else
                    {
                      if ($no_days_remaining >= 5 ) {?>
                      <small class="label label-success"><i class="fa fa-clock-o"></i><?php echo ' ',$no_days_remaining ,' days remaining';?>
                      </small>
                      <?php
                       }
                      else{
                          if ($no_days_remaining >= 2 ) {?>
                               <small class="label label-warning"><i class="fa fa-clock-o"></i><?php echo ' ',$no_days_remaining ,' days remaining';?>
                               </small>

                            <?php  }
                            else {
                                 if ($no_days_remaining <= 2) { ?>
                                 <small class="label label-danger"><i class="fa fa-clock-o"></i><?php echo ' ',abs($no_days_remaining) ,'days gone after Deadline..!!';?>
                                 </small>
                                  <?php  }
                               }
                          }
                    } ?>
                </li>
                  <?php } }?>
                
              </ul>
            </div>
            <!-- /.box-body -->
             <div class="box-footer clearfix no-border">
              <button type="button" value="add" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addNewWork"><i class="fa fa-plus"></i> Add New Work</button>
              <button type="submit" value="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
          </form>
          <!-- /.box -->

               <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Quick Email</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            
             <form role="form" name="emailForm" method="post" action="<?php echo base_url('/index.php/Welcome/sendMail');?>" onsubmit="return validateForm();">
             <div class="box-body">
              <div class="form-group">
                <input class="form-control" name="to" placeholder="To:" required="">
              </div>
                <div class="form-group">
                <input class="form-control" name="subject" placeholder="Subject:" required="">
              </div>
                  <textarea id="compose-textarea" name="message" class="form-control" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                   <div class="box-footer">
              <div class="pull-right">
                <button type="submit" value="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
              </div>
              <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
            </div>
            </div>
             </form>
                </div>



        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">


          <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Work Progress</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row margin">
                <!--<div class="col-sm-6">
                  <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="red">

                  <p>data-slider-id="red"</p>
                  
                </div>-->
                         <?php
                        if($empty!=0){
                           $final = $finalPercentage/$count1 * 100; ?>
                <div class="col-sm-12">
                  <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $final; ?>%">
                    <?php
                        echo intval($final);?>%                          
                    </div>
                  </div>
                </div>
                <?php }
                else{
                  ?>
                    <div class="col-sm-12">
                  <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%; color: blue; text-align: center;">
                    <?php
                        echo 0;?>%                          
                    </div>
                  </div>
                </div>
                <?php }?>
                
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
       <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">For sharing Ideas or Information click here!</h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="row margin">
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#shareIdea">Share Ideas</button>
                
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
       <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">For assigning new work to your employees click here!</h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
               <div class="row margin">
                <button type="button" value="assign" class="btn btn-primary btn-block" data-toggle="modal" data-target="#assignNewWork">Assign New Work</button>
                
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Glance through ideas! click here!</h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="row margin">
                <button type="button" value="submit"  class="btn btn-primary btn-block" data-toggle="modal" data-target="#readIdeas">Read ideas</button></a>
                
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

        </section>
        <!-- right col -->

      </div>
      <!-- /.row (main row) -->

          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>

  
 <!-- /.content-wrapper -->
       <script>
function validateForm() {
    var x = document.forms["emailForm"]["to"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid e-mail address");
        return false;
    }
}
</script>
      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>

 
 
<?php include 'add_new_work.php';?> 
<?php include 'assign_new_work.php';?> 
<?php include 'share_idea.php';?> 
<?php include 'read_ideas.php';?> 
<?php include 'edit_profile.php';?>
<?php include 'footer.php';?>
