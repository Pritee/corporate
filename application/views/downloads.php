 <?php include 'header.php';?>
 <div class="content-wrapper">
  <section class="content-header">
      <h1> Downloads </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Widgets</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     <?php if(!empty($files)){ foreach($files as $frow){ ?>
      <div class="col-md-4">
         <div class="box box-success box-solid">
            <div class="box-header with-border">
           
              <h3 class="box-title"><?php echo $frow['file_name']; ?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-body">
                <div class="file-box" style="display: inline-block;">
                 <div class="box-content">
                <div class="preview">
                    <embed src="<?php echo base_url().'uploads/files/'.$frow['file_name']; ?>">
                </div>
              <CENTER><a href="<?php echo base_url('/index.php/welcome/download/'.$frow['id']);?>" class="btn btn-primary btn-sm bg-green">Download</a></CENTER>
                 </div>
              </div>
           </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
       
        </div>
        <!-- ./col -->
    <?php } } ?>


 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>


<?php include 'footer.php';?>



