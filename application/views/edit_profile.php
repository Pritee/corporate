<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
            <form class="form-horizontal" role="form"  method="post" action="<?php echo base_url('/index.php/Welcome/updateProfile');?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><strong>Edit Your Profile</strong></h4>
      </div>
      <div class="modal-body">
        
          <CENTER><h5 class="modal-title" id="myModalLabel"><strong>Personal Details</strong></h5></CENTER>
              <div class="box-body">
               <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Full Name:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputName" name="name" placeholder=" FullName"> 
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email:</label>

                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail3" name="email" placeholder="Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputNumber" class="col-sm-2 control-label">Mob No.:</label>

                  <div class="col-sm-10">
                     <input type="text" id="inputNumber" class="form-control" name="phone" placeholder="Mobile No" maxlength="10" required/>
                  </div>
                </div>
                <div class="form-group">
                <label for="inputNumber" class="col-sm-2 control-label">DOB:</label>
                <div class=" col-sm-10">  
                  <input type="date" class="form-control" name="dob" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask placeholder="Date">
                </div>
                <!-- /.input group -->
              </div>
     
                 <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">NewPassword:</label>

                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password" id="inputPassword3" placeholder=" New Password">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <!--<div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Sign in</button>
              </div>-->
              <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" value="submit" class="btn btn-info pull-right">Save changes</button>
      </div>
            </form>

    </div>
  </div>
</div>