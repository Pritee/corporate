<?php include 'header.php';?>
<!-- Content Wrapper. Contains page content -->
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <?php foreach ($user_data as $key => $value) {?>
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary box-info ">
            <div class="box-body box-profile ">

              <div class="widget-user-image">

              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" alt="User profile picture">

              </div>
              <h3 class="profile-username text-center"><?php echo $value->username ?></h3>
              <b><p class="text-muted text-center"><?php echo $value->position ?></p></b>


                <li class="list-group-item">
                  <b>Salary</b> <a class="pull-right">INR. &nbsp;<?php echo $value->salary ?></a>  
                </li>
                <li class="list-group-item">
                  <b>Hired date</b> <a class="pull-right"><?php echo $value->hired_date ?></a>
                  
                </li>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
         
           
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">

              <h3 class="profile-username text-center">Actions</h3>
               <!--<button type=button class="btn btn-primary btn-block" data-toggle="modal" data-target="#edit_profile"><b>Edit Profile</b></button>-->
               <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal">Edit Profile</button>
              <br>
             </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>


          <!-- About Me Box -->
        <div class="col-md-9">
          <div class="box box-primary box-info box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i>Contact Infomation</strong>
              <br><br>
              <p class="text-muted">
                <b>Employee Name: </b><?php echo $value->name ?>
              </p>
              <p class="text-muted">
                <b>Mobile No: </b><?php echo $value->phone ?>
              </p>
              <p class="text-muted">
                <b>Email-ID: </b><?php echo $value->email ?>
              </p>

              <hr>
              <strong><i class="fa fa-map-marker margin-r-5"></i>Addtional Information</strong>
              <br><br>
              <p class="text-muted"><b>DOB: </b><?php echo $value->date_of_birth; ?></p>
              <p class="text-muted"><b>Address: </b><?php echo $value->address ;?></p>

              <hr>

              <strong><i class="fa fa-book margin-r-5"></i> Reputation points</strong>

              <p class="text-muted">
               <?php echo $value->reputation ;?>
              </p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        
      </div>
       <?php }?>
      <!-- /.row -->
      <!-- Button trigger modal -->




    </section>
    <!-- /.content -->
  </div>
      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>


 <?php include 'edit_profile.php';?>     
<?php include 'footer.php';?>
