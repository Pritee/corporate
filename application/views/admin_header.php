<!DOCTYPE html>
<html>
<?php
if (isset($this->session->userdata['logg'])) {
  $position = ($this->session->userdata['user_position']);
$username = ($this->session->userdata['logg']['username']);
} else {
header("location: http://localhost/corporate/index.php/login");
}
$data = ($this->session->userdata['message1']);
$message1= $data['message1'];


$data = ($this->session->userdata['message2']);
$message2 = $data['message2'];

$data = ($this->session->userdata['project_list']);
$project_list= $data['project_list'];


$data = ($this->session->userdata['recepient_list']);
$recepient_list= $data['recepient_list'];



$data = ($this->session->userdata['work_notification']);
$work_notification = $data['work_notification'];


if (isset($this->session->userdata['title'])) {
  $this->session->unset_userdata('title');
}


if (isset($this->session->userdata['sub_work'])) {
  $this->session->unset_userdata('sub_work');
}


if (isset($this->session->userdata['chat_id'])) {
  $this->session->unset_userdata('chat_id');
}

if (isset($this->session->userdata['receiver_name'])) {
  $this->session->unset_userdata('receiver_name');
}

?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Corporate World</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


  <!-- Ion Slider -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/ionslider/ion.rangeSlider.css')?>">
  <!-- ion slider Nice -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/ionslider/ion.rangeSlider.skinNice.css')?>">
  <!-- bootstrap slider -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/bootstrap-slider/slider.css')?>">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
   <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/AdminLTE.min.css')?>">
    <!-- Morris charts -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/morris/morris.css')?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/skins/_all-skins.min.css')?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/iCheck/flat/blue.css')?>">
 
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/datepicker/datepicker3.css')?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/daterangepicker/daterangepicker.css')?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>">
  <link href='//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.1.1/fullcalendar.min.css' rel='stylesheet' />
   <link href='//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.1.1/fullcalendar.print.css' rel='stylesheet' media='print' />
   <link href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
     <link href="<?php echo base_url();?>assets/css/bootstrap-timepicker.min.css" rel="stylesheet" />
      

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('/index.php/welcome/index')?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>LTE</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>COR</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="<?php echo base_url('/index.php/Welcome/getMessages') ?>" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <?php 

              $new_count=0;
               foreach ($message1 as $key => $value) {
                      $new_count +=1;
                }?>
              <span class="label label-success"><?php if ($new_count != 0) {
                echo $new_count;
                } ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header" data-toggle="modal" data-target="#addMessage"><i class="fa fa-plus"></i>New message</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <?php

                    foreach ($message1 as $row) {?>
                      <a style="background-color: lightgrey;" href="<?php echo base_url() ?>/index.php/Welcome/chat/<?php echo $row->chat_id ?>/<?php echo $row->created_by ?>">
                      <div class="pull-left">
                        <img src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        <?php echo $row->name ?>

                             <?php 
                      $now = time(); // or your date as well
                      $your_date = strtotime($row->create_date);
                      $datediff = $now - $your_date;

                       $no_days_gone = floor($datediff / (60 * 60 * 24));
                  if ($no_days_gone <=60) {?>
                        <small><i class="fa fa-clock-o"></i><?php echo  ' ',$no_days_gone , ' days ago';  ?></small>
                   <?php  
                 } else
                    {?>
                        <small><i class="fa fa-clock-o"></i><?php echo  $row->create_date  ?></small>
                   <?php  } ?>

                      </h4>
                      <p><?php echo $row->header ?></p>
                    </a>
                    <?php }?>
                     <?php 
                    foreach ($message2 as $row) {?>
                      <a href="<?php echo base_url() ?>/index.php/Welcome/chat/<?php echo $row->chat_id ?>/<?php echo $row->created_by ?>">
                      <div class="pull-left">
                        <img src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        <?php echo $row->name ?>

                             <?php 
                      $now = time(); // or your date as well
                      $your_date = strtotime($row->create_date);
                      $datediff = $now - $your_date;

                       $no_days_gone = floor($datediff / (60 * 60 * 24));
                  if ($no_days_gone <=60) {?>
                        <small><i class="fa fa-clock-o"></i><?php echo  ' ',$no_days_gone , ' days ago';  ?></small>
                   <?php  
                 } else
                    {?>
                        <small><i class="fa fa-clock-o"></i><?php echo  $row->create_date  ?></small>
                   <?php  } ?>
                      </h4>
                      <p><?php echo $row->header ?></p>
                    </a>
                    <?php }?>
                  </li>
                  <!-- end message -->
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">1</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="todo-list" >
                  <li>
                  <?php 
                  if (empty($work_notification)) {
                  echo 'empty';
                }
                else{

                  foreach ($work_notification as $key => $value) {?>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> <?php echo $value->name ,' has completed ', $value->work_title, ' job. Please confirm its completion.' ;?>
                    </a>
                    <?php } }?>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <img src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $username;?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src='<?php echo base_url(). "assets/images/user2-160x160.jpg"?>' class="img-circle" alt="User Image">

                <p>
                  <?php echo $username;?> - <?php echo $position;?>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('/index.php/welcome/profile')?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('/index.php/login/logout')?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src='<?php echo base_url(). "assets/images/user2-160x160.jpg"?>' class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <br>
          <p><?php echo $username ?></p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
         <li>
          <a href="<?php echo base_url('/index.php/welcome/index')?>">
            <i class="glyphicon glyphicon-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
       <li>
          <a href="<?php echo base_url('/index.php/welcome/registerEmployee')?>">
            <i class="glyphicon glyphicon-pencil"></i> <span>Register Employee</span>
          </a>
        </li>
      <li>
          <a href="<?php echo base_url('/index.php/welcome/employeeInformation')?>">
            <i class="glyphicon glyphicon-user"></i> <span>View Employee Info</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('/index.php/welcome/ideaInformation')?>">
            <i class="glyphicon glyphicon-pushpin"></i> <span>View Ideas Info</span>
          </a>
        </li>
        <li>
        <a href="<?php echo base_url('/index.php/Welcome/viewIdeaGraph');?>">
        <i class="glyphicon glyphicon-signal"></i>View Idea Graph</i></a>
        </li>
        <li>
          <a href="<?php echo base_url('/index.php/welcome/viewProjectGraph')?>">
            <i class="glyphicon glyphicon-star"></i> <span>View Projects Graph</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('/index.php/welcome/calendar')?>">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('/index.php/welcome/sentMail')?>">
            <i class="fa fa-envelope"></i> <span>Compose Mail</span>
          </a>
        </li>
         <li>
          <a href="<?php echo base_url('/index.php/welcome/aboutCompany')?>">
            <i class="glyphicon glyphicon-console"></i> <span>About Company</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <?php include 'new_message.php';?> 

