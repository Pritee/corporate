

  <?php include 'admin_header.php';?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header ">
      <h1>Employee Information</h1>
    </section>

    <!-- Main content -->
    <section class="content">
 
      <div class="box " >
      
        <div class="box-header with-border bg-green">
          <h3 class="box-title ">Add Employee Details</h3>
        </div>

          <div class="box-body">
 <form  role="form"  method="post" action="<?php echo base_url('/index.php/Welcome/employeeInfoDetails');?>">
      <!--<?php echo validation_errors() ?>
      <?php echo $captcha_return?>-->
  <dl class="dl-horizontal">

                     <div class="form-group">
      <dt><label>Select category :</label></dt>
      <dd><input type="radio" name="multiple" value="Manager"  <?php echo set_radio('multiple', 'Manager'); ?>/> Manager &nbsp;&nbsp;</dd>   
      <dd><input type="radio" name="multiple" value="TeamLeader"  <?php echo set_radio('multiple', 'TeamLeader'); ?>/> TeamLeader &nbsp;&nbsp;</dd>   

      <dd><input type="radio" name="multiple" value="Employee"  <?php echo set_radio('multiple', 'Employee'); ?>/> Employee &nbsp;&nbsp;</dd>   
      <dd><input type="radio" name="multiple" value="All"  <?php echo set_radio('multiple', 'All'); ?>/> All
      <br></dd>
    </div>

</dl>
      <div class="box-footer">
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
          <button type="submit" name="register" value="submit" class="btn btn-primary pull-right">Submit</button>
        </div>
      <br>

      <?php echo form_close()?>   
    </form>
      <!-- box-body end-->
      </div>
      <!-- box end-->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>

  <?php include 'footer.php';?>