


 <?php


$data =$this->session->userdata['myideas'];
$idea_list = $data['myideas'];


  include 'header.php';?>
 <div class="content-wrapper">
  <section class="content-header">
      <h1>
        IDEAS given by You..
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Ideas</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <?php foreach ($idea_list as $key => $value) {
     ?>
       <div class="col-md-4">
         <div class="box box-success box-solid bg-info">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $value->project_type;?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
             <div class="box-body" style="height: 200px;">
              <dl class="dl-horizontal">
                <dt>Idea ID</dt>
                <dd><?php echo $value->idea_id ;?></dd>
                <dt>Project Name</dt>
                <dd><?php echo $value->project_name ;?></dd>
                 <dt>Idea Subject :</dt>
                <dd><?php echo $value->subject ;?></dd>
                <dt>Idea Status</dt>
                 <dd><?php echo $value->status;?></dd>
                  <dt>to whom</dt>
                <dd><?php echo $value->to_whom ;?></dd>
                <?php if ($value->status == 'accept') {
                        ?>
                         <dt>Reputation earned</dt>
                         <dd>+2 points</dd>

               <?php }
               else if ($value->status == 'In Progress') {
                ?>
                           <dt>Reputation earned</dt>
                         <dd>+1 points</dd>              
              <?php   } else if ($value->status == 'reject') {?>
                               <dt>Reputation decrement</dt>
                                <dd>-1 points</dd>     
             <?php } ?>
               
              </dl>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
        <?php } ?>

 </section>
 </div>

<?php include 'footer.php';?>

