

  <?php include 'admin_header.php';?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header ">
      <h1>Register New Employee</h1>
    </section>

    <!-- Main content -->
    <section class="content">
 
      <div class="box " >
      
        <div class="box-header with-border bg-green">
          <h3 class="box-title ">Add Employee Details</h3>
        </div>

          <div class="box-body">
 <form  role="form"  method="post" action="<?php echo base_url('/index.php/Welcome/addNewEmployee');?>">
      <!--<?php echo validation_errors() ?>
      <?php echo $captcha_return?>-->
  <dl class="dl-horizontal">
    <div class="form-group">
      <dt><label>Unique_ID :</label></dt>
      <dd><input type="text" class="form-control" name="u_id" value="<?php echo set_value('Unique_Id') ?>" placeholder="Unique_Id" /></dd>
    </div>

    <div class="form-group">
      <dt><label>FULL Name :</label></dt>
      <dd><input type="text" class="form-control" name="name" value="<?php echo set_value('name') ?>" placeholder="Your Name" /></dd>
    </div>

    <div class="form-group">
      <dt><label>Username :</label></dt>
     <dd><input type="text" class="form-control" name="username" value="<?php echo set_value('username') ?>" placeholder="Username" /></dd>
    </div>

    <div class="form-group">
      <dt><label>Password :</label></dt>
      <dd><input type="password" name="password" class="form-control"  onchange="form.passconf.pattern = this.value; value="<?php echo set_value('password') ?>" placeholder="Password" /></dd>
   </div>

    <div class="form-group">
      <dt><label>Confirm Password :</label></dt>
      <dd><input type="password" name="passconf" class="form-control"  value="<?php echo set_value('passconf') ?>" placeholder="Confirm Password" /></dd>
    </div>

    <div class="form-group">
      <dt><label>Email_ID :</label></dt>
      <dd><input type="text" name="email" class="form-control" value="<?php echo set_value('email') ?>" placeholder="Email ID"/></dd>
    </div>

    <div class="form-group">
      <dt><label>Mobile :</label></dt>
      <dd><input type="text" name="phone" class="form-control" value="<?php echo set_value('phone') ?>" placeholder="Mobile No" maxlength="10"/></dd>
    </div>

    <div class="form-group">
      <dt><label>Address :</label></dt>
      <dd><textarea class="form-control" type="area" name="address" placeholder="Your Address" value="<?php echo set_value('address') ?>"></textarea></dd>
      </div>

      <div class="form-group">
      <dt><label>Position :</label></dt>
      <dd><select name="position" type="text" class="form-control">
      <option value="">Select Position</option>
      <option value="Admin" <?php echo set_select('position', 'Admin'); ?>>Admin</option>
      <option value="Manager" <?php echo set_select('position', 'Manager'); ?> >Manager</option>
      <option value="TeamLeader" <?php echo set_select('position', 'Team Leader'); ?> >Team Leader</option>
      <option value="Employee" <?php echo set_select('position', 'Employee'); ?> >Employee</option>
      </select></dd>
<br>
 <div class="form-group">
      <dt><label>Manager_ID :</label></dt>
      <dd><input type="text" class="form-control" name="m_id" value="<?php echo set_value('NULL') ?>" placeholder="Manager_Id" /></dd>
    </div>

     <div class="form-group">
      <dt><label>Team Leader_ID :</label></dt>
      <dd><input type="text" class="form-control" name="t_id" value="<?php echo set_value('NULL') ?>" placeholder="Team_Leader_Id" /></dd>
    </div>

    <div class="form-group">
      <dt><label>Birth_Date :</label></dt>
      <dd><input type="date" class="form-control" name="dob" value="<?php echo set_value('dob') ?>" placeholder="DOB" /></dd>
    </div>

     <div class="form-group">
      <dt><label>Reputation :</label></dt>
      <dd><input type="number" name="reputation" class="form-control" value="<?php echo set_value('reputation') ?>" placeholder="Reputation Pt."/></dd>
    </div>

     <div class="form-group">
      <dt><label>Hired_Date :</label></dt>
      <dd><input type="date" class="form-control" name="hired_date" value="<?php echo set_value('hired_date') ?>" placeholder="hired_date" /></dd>
    </div>

    <div class="form-group">
      <dt><label>Salary :</label></dt>
      <dd><input type="number" name="salary" class="form-control" value="<?php echo set_value('salary') ?>" placeholder="salary"/></dd>
    </div>

    <div class="form-group">
      <dt><label>Gender :</label></dt>
      <dd><input type="radio" name="gender" value="Male"  <?php echo set_radio('gender', 'Male'); ?>/> Male &nbsp;&nbsp;</dd>   
      <dd><input type="radio" name="gender" value="Female"  <?php echo set_radio('gender', 'Female'); ?>/>Female 
      <br></dd>
    </div>

    <div class="form-group">
     <dd><input type="checkbox" name="terms" value="1" <?php echo set_checkbox('terms', '1'); ?> />I agree to the Terms of Service and Privacy Policy
     </div></dd>
</dl>
      <div class="box-footer">
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
          <button type="submit" name="register" value="submit" class="btn btn-primary pull-right">Submit</button>
        </div>
      <br>

      <?php echo form_close()?>   
    </form>
      <!-- box-body end-->
      </div>
      <!-- box end-->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>

  <?php include 'footer.php';?>
    <?php include 'project_idea.php';?>
