

<?php


$data = ($this->session->userdata['increment_list']);
$increment_list= $data['increment_list'];


 ?>

  <?php include 'header.php';?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header ">
      <h1>Salary Increment</h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->

      <div class="box " >
       <?php foreach ($increment_list as $key => $value) {
              if ($value->unique_id == $increment_id) {   ?>
        <form role="form" method="post" action="<?php echo base_url() ?>/index.php/Welcome/salaryIncrement/<?php echo $value->unique_id ?>" >
        <div class="box-header with-border bg-black">
          <h3 class="box-title ">Employees Details</h3>
        </div>
           <div class="box-body">
              <dl class="dl-horizontal">
                <dt>Employee Name :</dt>
                <dd name="id" value="<?php echo $value->unique_id; ?>" ><?php echo $value->name; ?></dd>
                <hr>
                <dt>Designation :</dt>
                <dd><?php echo $value->position;?></dd>
                <hr>
                <dt>Total Salary :</dt>
                 <dd><?php echo $value->salary;?></dd>
          <hr>
                <dt>Increment In Salary :</dt>
                <dd>
                <div class="form-group">
                  <input type="text" class="form-control" name="salary" placeholder="Enter increment in Salary..." required >
                </div>
                </dd>
                <hr>
                <dt>Position Increment :</dt>
                <dd>
                <select name="position" type="text" class="form-control" pattern="\w+">
                <option value="" class="text">Select Position</option>
                <?php if ($value->position =='TeamLeader') {?>
                <option value="Manager">Manager</option>
                 <?php }else { ?>
                <option value="TeamLeader" >Team Leader</option>
                                <?php }?>

                </select>
                <br>

                </dd>
                        <button type="submit" value="submit" class="btn btn-primary btn-lg bg-black pull-right">Submit</button>
                        </dl>
                                              <?php } } ?>


            </div>

            <!-- /.box-body -->
        <!-- /.box-body -->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include 'footer.php';?>