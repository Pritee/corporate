
<?php 
 

$data = ($this->session->userdata['team_list']);
$team_list= $data['team_list'];

 ?>

 

 <style type="text/css">
   
   @media screen and (min-width: 768px){
    #assignedWork .modal-dialog {width: 900px;}
    
   }

   </style>
<!-- Modal -->
<div class="modal fade" id="assignNewWork" tabindex="-1" role="dialog" aria-labelledby="assignWorkLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <h4 class="modal-title" id="assignWorkLabel"><strong>Assign New Work</strong></h4>
      </div>
      <div class="modal-body">




 <div class="row">

        <div class="col-xs-12">

         <!-- form start -->
            <form enctype="multipart/form-data"  method="post" action="<?php echo base_url('/index.php/Welcome/uploadFile');?>">
             
          <div class="box-body">
            
                <div class="form-group">
                  <label>Work Title</label>
                  <input type="text" class="form-control" name="title" placeholder="Enter work Subject..." required="">
                </div>
                <div class="form-group">
                  <label>Work Description</label>
                  <textarea class="form-control" rows="3" name="descr"  placeholder="Enter Work Description..." required=""></textarea>
                </div>



                <div class="form-group">
                  <label>Work Duration</label>
                  <input type="number" class="form-control" name="days" placeholder="Enter number of days required to do the work..." required="">
                </div>


                  <div class="form-group">
                  <label>Select Recepient</label>
                  <select class="form-control" name="receiver" required="">
                    
                     <?php foreach ($team_list as $key => $value) {?>
                    <option value="<?php echo $value->unique_id; ?>" ><?php echo $value->name; ?></option>
                    <?php } ?>
                  </select>
                </div>
                
               <div class="form-group">
                    <label>Choose Files</label>
                    <input type="file" class="form-control" name="userFiles[]" multiple/>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
                 <input class="btn btn-primary" type="submit" name="fileSubmit" value="SUBMIT"/>
               

              </div>
 
            </form>
            </div>
            </div>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>-->
      </div>
    </div>
  </div>
</div>