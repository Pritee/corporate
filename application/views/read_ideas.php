<?php


$data = ($this->session->userdata['ideas']);
$idea_list= $data['ideas'];


 ?>


<!-- Modal -->
<div class="modal fade" id="readIdeas" tabindex="-1" role="dialog" aria-labelledby="readIdeasLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="box-title">Ideas on your Projects </h3>

        </div>
      
      <div class="modal-body">


      <!-- Small boxes (Stat box) -->
      <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 15px">Sr. No.</th>
                  <th>Project Name</th>
                  <th>Subject</th>
                  <th>days</th>
                </tr>
                <?php 

                 $idea_count = 0 ;
                if (empty($idea_list)) {
                  echo 'empty';
                }
                else{

                  foreach ($idea_list as $key => $value) { ?>
                 <tr>

                  <td><?php echo $idea_count= $idea_count + 1; ?>.</td>
                  <td><?php echo $value->project_name; ?></td>
                  <td> <a  style="cursor:pointer" href="<?php echo base_url() ?>/index.php/Welcome/viewIdea/<?php echo $value->idea_id ?>"><?php echo $value->subject; ?></a></td>
                
                  <td><?php 
                      $now = time(); // or your date as well
                      $your_date = strtotime($value->idea_date);
                      $datediff = $now - $your_date;

                       $no_days_gone = floor($datediff / (60 * 60 * 24));
                       echo $no_days_gone;
                   ?></td>
                </tr>
                <?php }} ?>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

      </div>
      
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>-->
      </div>
    </div>
  </div>
</div>
