<?php 
 
 $data=($this->session->userdata['project_list']);
 $project_list =$data['project_list'];




?>


  <?php include 'header.php';?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Details of Project
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box box-success box-solid">
        <?php foreach ($project_list as $key => $value) {
                  if ($value->project_id == $p_id) { 
                    $project_name =$value->project_name; ?>

        <div class="box-header with-border">
         <center> <h3 class="box-title" style="color: white"><b><i><?php echo $value->project_name; ?></i></b></h3></center>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
              <dl class="dl-horizontal">
                <dt>Project ID</dt>
                <dd>1</dd>
                <hr>
                <dt>Project Name</dt>
                <dd><?php echo $value->project_name; ?></dd>
                <hr>
                <dt>Short Desc.</dt>
                 <dd><?php echo $value->project_desc;?></dd>
          <hr>
                <dt>Project Timeline</dt>
                <dd>
                 <ul class="list-unstyled">
                  <li>Feb 12: Identifying team members</li>
                  <li> Feb 19: Project topics</li>
                  <li>Mar 5: Proposal due</li>
                  <li>Apr 2: Mid-term report due (data description, preliminary results)</li>
                  <li>Apr 23: Final report due</li>
                  <li>Apr 23: Project presentations in .ppt or .pptx format due</li>
                  <li>Apr 25 – May 2: Project presentations</li>
                 </ul>  
                </dd>
              </dl>
            </div>
            <!-- /.box-body -->
        <!-- /.box-body -->
        <?php if ($this->session->userdata('user_position')!='Manager') {?>
        <div class="box-footer">
          <button type="button" class="btn btn-primary btn-lg bg-navy pull-right" data-toggle="modal" data-target="#projectIdea">Give Idea or Solution</button>
        </div>
        <!-- /.box-footer-->
        <?php        
        }  } } ?>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
  
      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>

  <?php include 'project_idea.php';?>
  <?php include 'footer.php';?>