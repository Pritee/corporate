



<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/AdminLTE.min.css')?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/skins/_all-skins.min.css')?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/iCheck/flat/blue.css')?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/morris/morris.css')?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/datepicker/datepicker3.css')?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/daterangepicker/daterangepicker.css')?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <h2 style="padding-left: 30%">Chat Application...</h2></center>
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg4.jpg'); ?>'); padding-left: 20%;padding-top: 5%;padding-right: 10%;" >
          

            <div class="col-md-10">
              <!-- DIRECT CHAT -->
              <div class="box box-warning direct-chat direct-chat-warning">
                

                <div class="box-header with-border">
                <?php $receiver_name =$this->session->userdata('receiver_name') ;?>
                  <h3 class="box-title"><?php echo $receiver_name; ?></h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                      <i class="fa fa-comments"></i></button>
                    <a href="<?php echo base_url('/index.php/welcome/index')?>" class ="btn btn-box-tool"><i class="fa fa-times"></i></a>
                    </button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages">
                  <?php $id =$this->session->userdata('user_id') ;?>

                  <?php foreach ($chats as $row) {
                    if (($row->receiver_id )== $id) {?>
                    <?php $receiver_id = $row->created_by ; ?>


                     <!-- Message. Default to the left -->
                    <div class="direct-chat-msg">
                     <img class="direct-chat-img" src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" alt="message user image"><!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                       <?php echo $row->message_content ?>

                      </div>
                      <!-- /.direct-chat-text -->
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left"></span>
                        <span class="direct-chat-timestamp pull-left"><?php echo $row->create_date ?></span>
                      </div>
                      <!-- /.direct-chat-info -->

                    </div>
                    <!-- /.direct-chat-msg -->
                    <?php } else {?>
                    <?php $receiver_id = $row->receiver_id;?>
                    <!-- Message to the right -->
                    <div class="direct-chat-msg right">
                      
                      <img class="direct-chat-img" src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" alt="message user image"><!-- /.direct-chat-img -->
                      <div class="direct-chat-text" style="text-align: right">
                       <?php echo $row->message_content ?>
                      </div>
                      <!-- /.direct-chat-text -->
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right"></span>
                        <span class="direct-chat-timestamp pull-right"><?php echo $row->create_date ?></span>
                      </div>
                      <!-- /.direct-chat-info -->
                    </div>
                    <!-- /.direct-chat-msg -->
                    <?php } }?>

                  </div>
                  <!--/.direct-chat-messages-->

                  <!-- Contacts are loaded here -->

                  <!-- /.direct-chat-pane -->
                </div>
                <!-- /.box-body -->
                 <div class="box-footer">
    <form action=" <?php echo site_url()?>/Welcome/newLink1/<?php echo $receiver_id ?>" method="post">
                    <div class="input-group">
                    <div id= "chat_message">
                      <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                      </div>
                          <span class="input-group-btn">
                            <button type="submit" value="submit" class="btn btn-warning btn-flat">Send</button>
                          </span>
                    </div>
                  </form>
                </div>

                <!-- /.box-footer-->
              </div>
              <!--/.direct-chat -->
            </div>
            <!-- /.col -->
<script src="<?php echo base_url('assets/js/chat.js')?>"></script>

<script src="<?php echo base_url('plugins/jQuery/jquery-2.2.3.min.js')?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url('plugins/morris/morris.min.js')?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('plugins/sparkline/jquery.sparkline.min.js')?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('plugins/knob/jquery.knob.js')?>"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url('plugins/daterangepicker/daterangepicker.js')?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js')?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('plugins/fastclick/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/js/app.min.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/js/pages/dashboard.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/js/demo.js')?>"></script>

<script>
$(document).ready(function(){
        $("div").scrollTop(1000);
});
</script>

            </body>