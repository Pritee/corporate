<?php include 'admin_header.php';?>
<!-- Content Wrapper. Contains page content -->
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manager details
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <?php foreach ($info as $key => $value) {?>
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary box-info ">
            <div class="box-body box-profile ">

              <div class="widget-user-image">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" alt="User profile picture">
              </div>
              <h3 class="profile-username text-center"><?php echo $value->name; ?></h3>
              <b><p class="text-muted text-center"><?php echo $value->position; ?></p></b>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
         
           

        </div>


        
         <?php }?> 
      </div>
     
      <!-- /.row -->
      <!-- Button trigger modal -->




    </section>
    <!-- /.content -->
  </div>
      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>


 <?php include 'edit_profile.php';?>     
<?php include 'footer.php';?>
