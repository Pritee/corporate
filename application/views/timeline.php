 <?php include 'header.php';


$data = ($this->session->userdata['work_list']);
$work_list= $data['work_list'];


?>
 <div class="content-wrapper">
     <section class="content-header">
 <div class="col-md-12">
          <div class="nav-tabs-custom">

              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2014
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-envelope bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                      <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                      <div class="timeline-body">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                        quora plaxo ideeli hulu weebly balihoo...
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-primary btn-xs">Read more</a>
                        <a class="btn btn-danger btn-xs">Delete</a>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-user bg-aqua"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                      <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
                      </h3>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-yellow"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                      <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                      <div class="timeline-body">
                        Take me to your leader!
                        Switzerland is small and neutral!
                        We are more like Germany, ambitious and misunderstood!
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-green">
                          3 Jan. 2014
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-camera bg-purple"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

                      <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                      <div class="timeline-body">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                      </div>
                                          </div>


                <?php  $work_count = 0 ;
                if (empty($work_list)) {
                  echo 'empty';
                  # code...
                }
                else{

                  $count=0;
             foreach ($work_list as $key => $value) { 
                        $work_id = $value->work_id;
                        ?>
                        <li class="time-label">
                        <?php
                        $now = time(); // or your date as well
                      $your_date = strtotime($value->assign_date);
                      $datediff = $now - $your_date;

                       $no_days_gone = floor($datediff / (60 * 60 * 24));
                       $no_days_remaining = $value->days - $no_days_gone;
                       $date = $now + $no_days_remaining;
                       ?>
                        <span class="bg-green">
                        <?php  echo date("d F Y",strtotime("+$no_days_remaining days")); ?>
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-envelope bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                      
                  <?php

                  foreach ($work_notification as $key => $val) {
                          $work_notification_id =$val->work_id; 
                               if($work_id == $work_notification_id){ 
                                $count=1;
                   ?>
                   <h3 class="timeline-header"><a href="#"><?php echo $value->work_title ?></a> has to be completed... </h3>

                      <div class="timeline-body">

                  <span class="text"><a  style="cursor:pointer ; color:green;" href="<?php echo base_url() ?>index.php/Welcome/work/<?php echo $value->work_id ?>"><?php echo $value->descr; ?></a></span>

                  <?php

                   }
                }

                   if($count!=1){
                    ?>        
                    <h3 class="timeline-header"><?php echo $value->work_title ?></a> has to be completed... </h3>
                      <div class="timeline-body">

                  <span class="text"><a  style="cursor:pointer" href="<?php echo base_url() ?>index.php/Welcome/work/<?php echo $value->work_id ?>"><?php echo $value->descr ; ?></a></span>

                <?php
                  }
                            $count=0;
                   ?>

                  <!-- Emphasis label -->
                   <?php 
                      $now = time(); // or your date as well
                      $your_date = strtotime($value->assign_date);
                      $datediff = $now - $your_date;

                       $no_days_gone = floor($datediff / (60 * 60 * 24));
                       $no_days_remaining = $value->days - $no_days_gone;
                  if ($no_days_remaining >=10) {?>
                   <small class="label label-primary"><i class="fa fa-clock-o"></i><?php echo '    ',$no_days_remaining ,' days remaining';?>
                  </small>
                   <?php  
                 } else
                    {
                      if ($no_days_remaining >= 5 ) {?>
                      <small class="label label-success"><i class="fa fa-clock-o"></i><?php echo '    ',$no_days_remaining ,' days remaining';?>
                      </small>
                      <?php
                       }
                      else{
                          if ($no_days_remaining >= 2 ) {?>
                               <small class="label label-warning"><i class="fa fa-clock-o"></i><?php echo '    ',$no_days_remaining ,' days remaining';?>
                               </small>

                            <?php  }
                            else {
                                 if ($no_days_remaining <= 2) { ?>
                                 <small class="label label-danger"><i class="fa fa-clock-o"></i><?php echo '    ',abs($no_days_remaining) ,'days gone after Deadline..!!';?>
                                 </small>
                                  <?php  }
                               }
                          }
                    } ?>
                  <!-- General tools such as edit or delete-->
                                     </div>

                 </div>
                </li>
                  <?php } }?>
                
                  </li>
                  <!-- END timeline item -->
                  <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                  </li>
                </ul>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        <!-- /.col -->

 </section>
 </div>

      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>
<?php include 'footer.php';?>