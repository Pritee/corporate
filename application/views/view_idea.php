<!-- Modal -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


  <!-- Ion Slider -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/ionslider/ion.rangeSlider.css')?>">
  <!-- ion slider Nice -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/ionslider/ion.rangeSlider.skinNice.css')?>">
  <!-- bootstrap slider -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/bootstrap-slider/slider.css')?>">
<!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="<?php echo base_url('plugins/fullcalendar/fullcalendar.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('plugins/fullcalendar/fullcalendar.print.css')?>" media="print">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/AdminLTE.min.css')?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/skins/_all-skins.min.css')?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/iCheck/flat/blue.css')?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/morris/morris.css')?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/datepicker/datepicker3.css')?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/daterangepicker/daterangepicker.css')?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


 <style type="text/css">
   
   @media screen and (min-width: 768px){
    .row {width: 900px;} 
   }
 </style>
</head>

<body style="background-image:url('<?php echo base_url('assets/images/bg4.jpg'); ?>');" >
<!-- Modal -->

 <div class="row " style="position: relative; left: 20%; top : 5%; background-color: #fffbbb; border-style: outset;">

 <div class="modal-header" style="background-color:#b2b2ff">
       <a href="<?php echo base_url('/index.php/welcome/index')?>" > <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></a>
        
         <h4 class="modal-title" id="myModalLabel"><strong><?php foreach ($idea_data as $key => $value) {
    echo $value->project_name;
        }
?></strong></h4>
      </div>

      <div class="modal-body">
        <?php foreach ($idea_data as $key => $value) {?>
         <!-- form start -->

         <div class="col-xs-12">
          <div class="box box-primary ">
            <div class="box-header">
             
              <div class="box-body">
              <div class="form-group">
                  <strong>Subject :</strong>
                  <p value="<?php echo $value->idea_id; ?>"><?php echo $value->subject; ?></p>
                </div>
                 <hr>
                <div class="form-group">
                  <strong>code :</strong>
                  <p><?php echo $value->code; ?></p>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="modal-footer">
                <button type="submit" class="btn btn-info pull-left"><a href="<?php echo base_url() ?>/index.php/Welcome/rejectIdea/<?php echo $value->idea_id ?>" style="color: white">Reject</a></button>   
                <button type="submit" class="btn btn-info pull-right"><a href="<?php echo base_url() ?>/index.php/Welcome/acceptIdea/<?php echo $value->idea_id ?>" style="color: white">Accept</a></button>        
             </div>
 
            <?php }?>
            </div>

            </div>
      </div>
      
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>-->
        </div>
      </div>

</body>