<?php 
 
 $data=($this->session->userdata['project_list']);
 $project_list =$data['project_list'];




?>


 <?php include 'header.php';?>
 <div class="content-wrapper">
     <section class="content-header">
      <h1>
        Current
        <small>Projects</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Current Projects</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <?php foreach ($project_list as $key => $value) {
     if ($value->project_type=='Current Project') { ?>
       <div class="col-md-6">
         <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b><?php echo $value->project_name; ?></b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
             <div class="box-body">
              <dl class="dl-horizontal">
                <dt>Project ID</dt>
                <dd><?php echo $value->project_id; ?></dd>
                <dt>Project Name</dt>
                <dd><?php echo $value->project_name; ?></dd>
              </dl>
                <CENTER><a href="<?php echo base_url()?>index.php/Welcome/detailProject/<?php echo $value->project_id ?>"><button type="button" class="btn btn-primary btn-lg bg-green">View Detailed Project</button></a></CENTER>
 
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <?php       }     } ?>

        <!-- ./col -->
         
 </section>
 </div>

      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>

<?php include 'footer.php';?>

