

 <style type="text/css">
   
   @media screen and (min-width: 768px){
    #assignedWork .modal-dialog {width: 900px;}
     
   }

   </style>
<!-- Modal -->
<div class="modal fade" id="addNewAdminWork" tabindex="-1" role="dialog" aria-labelledby="assignWorkLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <h4 class="modal-title" id="assignWorkLabel"><strong>Add New Work</strong></h4>
      </div>
      <div class="modal-body">




 <div class="row">

        <div class="col-xs-12">

         <!-- form start -->

            <form enctype="multipart/form-data"  method="post" action="<?php echo base_url('/index.php/Welcome/addNewAdminWork');?>">             
              <div class="box-body">
            
                <div class="form-group">
                  <label>Work Title</label>
                  <input type="text" class="form-control" name="title" placeholder="Enter work Subject..." required="">
                </div>
              </div>
              <!-- /.box-body -->
              <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
                 <input class="btn btn-primary" type="submit" name="fileSubmit" value="SUBMIT"/>
               
              </div>
 
            </form>
            </div>
            </div>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>-->
      </div>
    </div>
  </div>
</div>