<!-- Modal -->
<div class="modal fade" id="shareIdea" tabindex="-1" role="dialog" aria-labelledby="shareIdeaLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <h4 class="modal-title" id="shareIdeaLabel"><strong>Share Your Ideas</strong></h4>
      </div>
      <div class="modal-body">
         <!-- form start -->
            <form role="form"  method="post" action="<?php echo base_url('/index.php/Welcome/shareIdea');?>">
             
              <div class="box-body">
              <div class="form-group">
                  <label>Select Project Name</label>
                  <select value="" class="form-control" name="project_id" required="">
                     <?php foreach ($project_list as $key => $value) {?>
                    <option value="<?php echo $value->project_id; ?>" ><?php echo $value->project_name ,' (',$value->project_type,')'; ?></option>
                    <?php } ?>
                  </select>
                </div>
                  <div class="form-group">
                  <label>Select Recepient</label>
                  <select class="form-control" name="to_whom" required="">
                  <?php if($this->session->userdata('user_position')=='TeamLeader'){ ?>
                    <option value="CEO" >CEO</option>
                    <option value="Manager" >Manager</option>
                    <?php } else { ?>
                    <option value="Manager" >Manager</option>
                    <option value="TeamLeader" >Team Leader</option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Subject</label>
                  <input type="text" class="form-control" name="subject" placeholder="Enter Subject..." required="">
                </div>
                <div class="form-group">
                  <label>Idea/Code/Solution</label>
                  <textarea class="form-control" rows="9" name="code"  placeholder="Enter idea/code/solution..." required=""></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="exampleInputFile">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="idea" value="submit" class="btn btn-primary">Submit</button>
              </div>
 
            </form>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>-->
      </div>
    </div>
  </div>
</div>