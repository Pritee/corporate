<?php include 'admin_header.php';?>
<?php 
$data = ($this->session->userdata['work_list']);
$work_list= $data['work_list'];

$data = ($this->session->userdata['work_notification']);
$work_notification= $data['work_notification'];

?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    

    <!-- Main content -->
    <section class="content">
    <section class="content-header">
      <h1>
        Dashboard</h1>
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Ideas</h3>

              <p>Ideas collection</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo base_url('/index.php/Welcome/ideaInformation');?>" class="small-box-footer">View Ideas <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Members</h3>

              <p>Employee Information</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url('/index.php/Welcome/employeeInformation');?>" class="small-box-footer">Employee Information <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Register</h3>

              <p>User Registrations</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url('/index.php/Welcome/registerEmployee');?>" class="small-box-footer">Add Employees <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Graph</h3>

              <p>Idea's Graph</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="<?php echo base_url('/index.php/Welcome/viewIdeaGraph');?>" class="small-box-footer">View Idea Graph <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
        

       
  <!-- TO DO List -->
          
           <form role="form"  method="post" action="<?php echo base_url('/index.php/Welcome/submitAdminWork');?>">
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">To Do List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="todo-list">
                <?php  $work_count = 0 ;
             foreach ($work_list as $key => $value) { 
                        $work_id = $value->id;
                        ?>
                                  <li>     
                  <!-- checkbox -->
                  <input type="checkbox" value="<?php echo $value->id; ?>" name="work_id[]";>

                   
                  <!-- todo text -->

                  <?php

                    ?>
                    <span class=""><?php echo $work_count= $work_count + 1; ?>.
                  </span>
                  <span class="text"><a  style="cursor:pointer" ><?php echo $value->work_title ?></a></span>

                <?php
                   ?>

                  <!-- Emphasis label -->
                   <?php 
                      $now = time(); // or your date as well
                      $your_date = strtotime($value->assign_date);
                      $datediff = $now - $your_date;

                       $no_days_gone = floor($datediff / (60 * 60 * 24));
                  if ($no_days_gone >=10) {?>
                   <small class="label label-primary"><i class="fa fa-clock-o"></i><?php echo ' ',$no_days_gone ,' days gone';?>
                  </small>
                   <?php  
                 } else
                    {
                      if ($no_days_gone >= 5 ) {?>
                      <small class="label label-success"><i class="fa fa-clock-o"></i><?php echo ' ',$no_days_gone ,' days gone';?>
                      </small>
                      <?php
                       }
                      else{
                          if ($no_days_gone >= 2 ) {?>
                               <small class="label label-warning"><i class="fa fa-clock-o"></i><?php echo ' ',$no_days_gone ,' days gone';?>
                               </small>

                            <?php  }
                          }
                    } ?>
                </li>
                  <?php } ?>
                
              </ul>
            </div>            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <button type="button" value="add" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addNewAdminWork"><i class="fa fa-plus"></i> Add To Do</button>
              <button type="submit" value="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
          </form>
          <!-- /.box -->

          <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Quick Email</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            
             <form role="form" name="emailForm" method="post" action="<?php echo base_url('/index.php/Welcome/sendMail');?>" onsubmit="return validateForm();">
             <div class="box-body">
              <div class="form-group">
                <input class="form-control" name="to" placeholder="To:" required="">
              </div>
                <div class="form-group">
                <input class="form-control" name="subject" placeholder="Subject:" required="">
              </div>
                  <textarea id="compose-textarea" name="message" class="form-control" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                   <div class="box-footer">
              <div class="pull-right">
                <button type="submit" value="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
              </div>
              <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
            </div>
            </div>
             </form>
                </div>


        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

               <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header with border red ">
              <h3 class="box-title">For assigning new Project to Manager click here!</h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="row margin">
                <button type="button" value="assign" class="btn btn-info btn-block" data-toggle="modal" data-target="#assignNewWork">Assign New Project</button>
                
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.content-wrapper -->
   <script>
function validateForm() {
    var x = document.forms["emailForm"]["to"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid e-mail address");
        return false;
    }
}
</script>

  
      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>


<?php include 'add_new_admin_work.php';?>
<?php include 'assign_new_project.php';?>
<?php include 'footer.php';?>
