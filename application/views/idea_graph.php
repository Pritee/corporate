<?php include 'admin_header.php';



?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    

    <!-- Main content -->
    <section class="content">
    <section class="content-header">


      <!-- Main row -->
      <div class="row">
       
              
           <!-- DONUT CHART -->
   
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Donut Chart</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


         


        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.content-wrapper -->
  
      <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="">GANESH ZORE & SADHNA SINGH</a>.</strong> All rights
    reserved.
  </footer>

<?php include 'footer.php';?>
<!-- page script -->
<script>
 //DONUT CHART
    var donut = new Morris.Donut({
      element: 'sales-chart',
      resize: true,
      colors: ["#3c8dbc", "#f56954", "#00a65a","#FFD433"],
      data: [ <?php foreach ($idea_graph as $key => $value) {?>
        {label: '<?php echo $value->status; ?>', value: <?php echo $value->count1; ?>},
     <?php } ?>
      ],
      hideHover: 'auto'
    });
</script>


