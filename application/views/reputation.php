<?php


$data = ($this->session->userdata['increment_list']);
$increment_list= $data['increment_list'];


 ?>


<!-- Modal -->
<div class="modal fade" id="increment" tabindex="-1" role="dialog" aria-labelledby="readIdeasLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="box-title">Increments of Employees </h3>

        </div>
      
      <div class="modal-body">


      <!-- Small boxes (Stat box) -->
      <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 15px">Sr. No.</th>
                  <th>Name</th>
                  <th>Position</th>
                </tr>
                <?php 

                 $increment_count = 0 ;
                if (empty($increment_list)) {
                  echo 'empty';
                }
                else{

                  foreach ($increment_list as $key => $value) { ?>
                 <tr>

                  <td><?php echo $increment_count= $increment_count + 1; ?>.</td>
                  <td> <a  style="cursor:pointer" href="<?php echo base_url() ?>/index.php/Welcome/increments/<?php echo $value->unique_id ?>"><?php echo $value->name; ?></a></td>
                  <td><?php echo $value->position; ?></td>

                </tr>
                <?php }} ?>

              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>
          </div>
          <!-- /.box -->

      </div>
      
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>-->
      </div>
    </div>
  </div>
</div>