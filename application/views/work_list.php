


<?php 
$data = ($this->session->userdata['title']);
$title= $data['title'];

$data = ($this->session->userdata['sub_work']);
$sub_work= $data['sub_work'];


$data = ($this->session->userdata['work_notification']);
$work_notification = $data['work_notification'];

 ?>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


  <!-- Ion Slider -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/ionslider/ion.rangeSlider.css')?>">
  <!-- ion slider Nice -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/ionslider/ion.rangeSlider.skinNice.css')?>">
  <!-- bootstrap slider -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/bootstrap-slider/slider.css')?>">
<!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="<?php echo base_url('plugins/fullcalendar/fullcalendar.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('plugins/fullcalendar/fullcalendar.print.css')?>" media="print">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/AdminLTE.min.css')?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/skins/_all-skins.min.css')?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/iCheck/flat/blue.css')?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/morris/morris.css')?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/datepicker/datepicker3.css')?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/daterangepicker/daterangepicker.css')?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


 <style type="text/css">
   
   @media screen and (min-width: 768px){
    .row {width: 900px;} 
   }
 </style>
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg4.jpg'); ?>');" >
<!-- Modal -->



 <div class="row " style="position: relative; left: 20%; top : 5%; background-color: #fff; width: 60%; border-style: outset;">

      <div class="modal-header" style="background-color: #b2b2ff;">
       <a href="<?php echo base_url('/index.php/welcome/index')?>" > <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></a>
        
        <h4 class="modal-title" id="assignWorkLabel"><strong><center><?php foreach ($title as $key => $value) {
         echo $value->work_title; }?></center></strong></h4>
      </div>

     <div class="modal-body">
        <div class="col-xs-6">
          <div class="box box-primary" style="height: 100px">
            <div class="box-header">
           <strong><i class="fa fa-file-text-o margin-r-5"></i> Short Description </strong>

              <p><?php foreach ($title as $key => $value) {
         echo $value->descr; ?></p>
              </div>
              </div>
        </div>




          <div class="col-xs-6">
            <div class="box box-primary">
            <div class="box-header" style="height: 100px">
          <strong><i class="fa fa-file-text-o margin-r-5"></i> Attachments </strong>
          <br><br>
            <div class="box-content">
              <a href="<?php echo base_url()?>index.php/Welcome/download/<?php echo $value->file_name ?>" class="btn btn-primary btn-sm bg-green">Download</a>&nbsp;&nbsp;<?php echo $value->file_name; } ?>
            </div>
            </div>
            </div>
          </div>
        </div>





        <div class="col-xs-12">
                <!-- TO DO List -->


      <form role="form"  method="post" action="<?php echo base_url('/index.php/Welcome/markAsCompleteWork');?>">
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Work Assigned for these Project are </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="todo-list">
            <?php  $work_count = 0 ;

               $work_count = 0 ;
                if (empty($sub_work)) {
                  echo 'You have not assign any work for these project.'; ?>

                  <div class="box-footer clearfix no-border">
              <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#assignWork"><i class="fa fa-plus"></i> Assign New Work</button>
            </div>
          </div>

                  <?php 
                }
                else{
                  $count=0;
             foreach ($sub_work as $key => $value) { 
                        $sub_work_id = $value->work_id;
                        ?>
                                  <li>     
                  <!-- checkbox -->
                  <input type="checkbox" value="<?php echo $value->work_id; ?>" name="work_id[]";>

                   
                  <!-- todo text -->

                  <?php

                  foreach ($work_notification as $key => $val) {
                          $work_notification_id =$val->work_id; 
                               if($sub_work_id == $work_notification_id){ 
                                $count=1;
                   ?>
                   <span class="" style="color:green;"><?php echo $work_count= $work_count + 1; ?>.
                  </span>
                  <span class="text" style="cursor:pointer; color:green;" ><?php echo $value->work_title ?></span>

                  <?php

                   }
                }

                   if($count!=1){
                    ?>
                    <span class=""><?php echo $work_count= $work_count + 1; ?>.
                  </span>
                  <span class="text" style="cursor:pointer;" ><?php echo $value->work_title ?></span>

                  <?php
                  }
                            $count=0;
                   ?>
                  <!-- Emphasis label -->
                   <?php 
                      $now = time(); // or your date as well
                      $your_date = strtotime($value->assign_date);
                      $datediff = $now - $your_date;

                       $no_days_gone = floor($datediff / (60 * 60 * 24));
                       $no_days_remaining = $value->days - $no_days_gone;
                  if ($no_days_remaining >=10) {?>
                   <small class="label label-primary"><i class="fa fa-clock-o"></i><?php echo $no_days_remaining ,' days remaining';?>
                  </small>
                   <?php  
                 } else
                    {
                      if ($no_days_remaining >= 5 ) {?>
                      <small class="label label-success"><i class="fa fa-clock-o"></i><?php echo $no_days_remaining ,' days remaining';?>
                      </small>
                      <?php
                       }
                      else{
                          if ($no_days_remaining >= 2 ) {?>
                               <small class="label label-warning"><i class="fa fa-clock-o"></i><?php echo $no_days_remaining ,' days remaining';?>
                               </small>

                            <?php  }
                            else {
                                 if ($no_days_remaining <= 2) { ?>
                                 <small class="label label-danger"><i class="fa fa-clock-o"></i><?php echo ' ',abs($no_days_remaining) ,'days gone after Deadline..!!';?>
                                 </small>
                                  <?php  }
                               }
                          }
                    } ?>
                </li>

                    <?php
                  }
                 ?>
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#assignWork"><i class="fa fa-plus"></i> Assign New Work</button>
              <button type="submit" value="submit" class="btn btn-default pull-right">Mark As completed</button>
            </div>
          </div>
          <?php }?>
          </form>

          </div>
          <!-- /.box -->

 
        </div>


            </div>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>-->
      </div>
      
<?php include 'footer.php';?>

<?php include 'assign_work.php';?> 
