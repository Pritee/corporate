<?php
class CI_auth extends CI_Model {

function __construct()
{
parent::__construct();
$this->load->library('session');
$this->load->database();
$this->load->helper('url');
$this->load->model(array('CI_encrypt'));



}

function process_login($login_array_input = NULL){
if(!isset($login_array_input) OR count($login_array_input) != 2)
return false;
//set its variable
$username = $login_array_input[0];
$password = $login_array_input[1];
// select data from database to check user exist or not?
$query = $this->db->query("SELECT * FROM `users1` WHERE `username`= '".$username."' LIMIT 1");
if ($query->num_rows() > 0)
{
$row = $query->row();
$user_id = $row->unique_id;
$user_name = $row->username;
$user_pass = $row->password;
$user_salt = $row->salt;
$user_photo = $row->photo;
$user_position =$row->position;
$reputation =$row->reputation;

if($this->CI_encrypt->encryptUserPwd( $password,$user_salt) === $user_pass){
  $session_data = array(
          'username' => $user_name,
          );
$this->session->set_userdata('logg', $session_data );
$this->session->set_userdata('user_position', $user_position );
$this->session->set_userdata('user_name', $user_name );
$this->session->set_userdata('user_id', $user_id );
$this->session->set_userdata('reputation', $reputation );






return true;
}
return false;
}
return false;
}
function check_logged(){
return ($this->session->userdata('user_position'))?TRUE:FALSE;
}


function logged_id(){
return ($this->check_logged())?$this->session->userdata('user_name'):'';
}

function profile()
  {
    $query = $this->db->get_where('users1', array('username' => $this->session->userdata('user_name')))->result();
    $data['user_data'] =$query;
    return $data;
  }



  function updateProfile($name,$email,$phone,$dob,$password,$rand_salt,$user_id){   

    $query = $this->db->query("UPDATE `users1`set name='".$name."' , email ='".$email."' ,phone ='".$phone."' ,date_of_birth ='".$dob."' ,password='".$password."' ,salt='".$rand_salt."' WHERE unique_id='".$user_id."' ");
     // insert data into `students table` 
    if ($query > 0) {
         return true;
         }
         else {
         return false;
         }
  }


 function insertIdea($idea){  
    $this->db->insert('idea', $idea); // insert data into `students table` 
    if ($this->db->affected_rows() > 0) {
         return true;
         }
         else {
         return false;
         }
  }


 function insertEmail($email){  
    $this->db->insert('email', $email); // insert data into `students table` 
    if ($this->db->affected_rows() > 0) {
         return true;
         }
         else {
         return false;
         }
  }

   function readIdea()
   {
 $user_position= $this->session->userdata('user_position');
 $id= $this->session->userdata('user_id');

  if ($user_position =='Manager') {

      $query = $this->db->query("SELECT projects.assign_manager_id ,idea.idea_id,idea.idea_date,projects.project_name,idea.project_id,idea.subject,idea.code FROM `idea` JOIN `projects` WHERE idea.to_whom='Manager' AND idea.project_id=projects.project_id AND (idea.project_id IN (SELECT project_id FROM projects WHERE assign_manager_id='".$id."') AND idea.status IN ('In Progress','send')) ORDER BY idea.idea_date")->result();


   $this->session->unset_userdata('ideas');
   $data['ideas'] = $query ;
   $this->session->set_userdata('ideas', $data );


    $query2= $this->db->query("SELECT unique_id, name , position,salary FROM  `users1` WHERE reputation_status='I' AND ( team_leader_id IN (SELECT unique_id FROM users1 WHERE manager_id ='".$id."') OR unique_id IN (SELECT unique_id FROM users1 WHERE manager_id ='".$id."'))")->result();
   $this->session->unset_userdata('increment_list');
   $data['increment_list'] = $query2;
   $this->session->set_userdata('increment_list',$data);
 }
 else{


   $id= $this->session->userdata('user_id');
      $query = $this->db->query("SELECT projects.assign_manager_id ,idea.idea_id,idea.idea_date,projects.project_name,idea.project_id,idea.subject,idea.code FROM idea join projects WHERE projects.project_id=idea.project_id AND idea.to_whom='TeamLeader' AND projects.project_id IN (SELECT DISTINCT work_assign.project_id FROM work_assign JOIN projects WHERE receiver_id='".$id."' AND projects.project_id=work_assign.project_id) AND idea.status='send' ORDER BY idea.idea_date")->result();


   $this->session->unset_userdata('ideas');
   $data['ideas'] = $query ;
   $this->session->set_userdata('ideas', $data );

 }

    $query9 = $this->db->query("SELECT projects.project_name,idea.subject,idea.code,projects.project_id,projects.project_type,idea.idea_id,idea.status,idea.to_whom FROM `idea` JOIN `projects` WHERE projects.project_id=idea.project_id AND idea.ID = '".$id."' ")->result();
    $this->session->unset_userdata('myideas');
    $data['myideas'] = $query9;
    $this->session->set_userdata('myideas',$data );

      
   }

function viewIdea($idea_id){

  $query=$this->db->query("SELECT idea.subject,idea.code,idea.idea_id,projects.project_name FROM `idea` JOIN `projects` WHERE idea.project_id=projects.project_id AND idea_id='".$idea_id."'")->result();
  return $query;

}


function ideaAcceptResult($idea_id){
 $user_position= $this->session->userdata('user_position');
  if ($user_position =='Manager') {

    $query=$this->db->query("UPDATE `idea` SET status='accept',result_date ='".date('Y-m-d')."' WHERE idea_id='".$idea_id."'");

    $query1=$this->db->query("UPDATE `users1` SET reputation=reputation+2 WHERE unique_id IN (SELECT ID FROM `idea` WHERE idea_id='".$idea_id."')");

    $query =$this->db->query(" SELECT reputation , unique_id from users1 WHERE unique_id IN (SELECT ID FROM `idea` WHERE idea_id='".$idea_id."')")->result();
            foreach ($query as $key => $value) {
              if ($value->reputation >= '25') {

    $query1=$this->db->query("UPDATE `users1` SET reputation='0', reputation_status='I' WHERE unique_id IN (SELECT ID FROM `idea` WHERE idea_id='".$idea_id."')");
              }
            }

    }
    else{

     $query=$this->db->query("UPDATE `users1` SET reputation=reputation+1 WHERE unique_id IN (SELECT ID FROM `idea` WHERE idea_id='".$idea_id."')");

     $query1=$this->db->query("UPDATE `idea` SET to_whom='Manager', status='In Progress' WHERE  idea_id='".$idea_id."'");

       $query =$this->db->query(" SELECT reputation , unique_id from users1 WHERE unique_id IN (SELECT ID FROM `idea` WHERE idea_id='".$idea_id."')")->result();
            foreach ($query as $key => $value) {
              if ($value->reputation >= '25') {

    $query1=$this->db->query("UPDATE `users1` SET reputation='0', reputation_status='I' WHERE unique_id IN (SELECT ID FROM `idea` WHERE idea_id='".$idea_id."')");
              }
            }


    }

}


function ideaRejectResult($idea_id){

 $user_position= $this->session->userdata('user_position');
  if ($user_position =='Manager') {

    $query=$this->db->query("UPDATE `idea` SET status='reject', result_date ='".date('Y-m-d')."' WHERE idea_id='".$idea_id."'");

    }
    else{

          $query=$this->db->query("UPDATE `idea` SET status='reject' WHERE idea_id='".$idea_id."'");

           $query1=$this->db->query("UPDATE `users1` SET reputation=reputation-1 WHERE unique_id IN (SELECT ID FROM `idea` WHERE idea_id='".$idea_id."')");



    }

}


function getMessage(){
   $id= $this->session->userdata('user_id');
   $query = $this->db->query("SELECT message_list.created_by,users1.name,message_list.chat_id,message_list.header,message_list.create_date FROM `users1` JOIN `message_list` WHERE message_list.created_by=users1.unique_id AND message_list.message_status='send' AND message_list.chat_id IN (SELECT DISTINCT message_list.chat_id FROM `message_list` JOIN `messages` WHERE message_list.chat_id=messages.chat_id AND messages.receiver_id='".$id."')ORDER BY message_list.create_date DESC")->result();

   $this->session->unset_userdata('message1');
   $data['message1'] = $query ;
   $this->session->set_userdata('message1', $data );


   $query4 = $this->db->query("SELECT message_list.created_by,users1.name,message_list.chat_id,message_list.header,message_list.create_date FROM `users1` JOIN `message_list` WHERE message_list.created_by=users1.unique_id AND message_list.message_status='received' AND message_list.chat_id IN (SELECT DISTINCT message_list.chat_id FROM `message_list` JOIN `messages` WHERE message_list.chat_id=messages.chat_id AND messages.receiver_id='".$id."')ORDER BY message_list.create_date DESC")->result();

   $this->session->unset_userdata('message2');
   $data['message2'] = $query4 ;
   $this->session->set_userdata('message2', $data );



$query1 = $this->db->query("SELECT work_assign.work_id,work_assign.work_title,users1.name FROM users1 JOIN work_assign WHERE users1.unique_id=work_assign.receiver_id AND work_assign.work_status='submitted' AND work_assign.sender_id='".$id."'")->result();
$data['work_notification'] = $query1;
$this->session->set_userdata('work_notification',$data);


$query2 = $this->db->query("SELECT * FROM `projects`")->result();
$data['project_list'] = $query2;
$this->session->set_userdata('project_list',$data);



 $user_position= $this->session->userdata('user_position');
  if ($user_position =='Manager') {
        $query3 =$this->db->query("SELECT unique_id, name FROM `users1` WHERE team_leader_id IN (SELECT unique_id FROM users1 WHERE manager_id = '".$id."') OR unique_id IN (SELECT unique_id FROM users1 WHERE manager_id ='".$id."')  ")->result();
          $this->session->unset_userdata('message_list');
       $data['message_list'] =$query3 ;
      $this->session->set_userdata('message_list',$data);


    }
  else{
     if ($user_position =='TeamLeader') {
        $query3 =$this->db->query("SELECT unique_id,name FROM `users1` WHERE unique_id IN (SELECT unique_id FROM users1 WHERE team_leader_id ='".$id."') OR unique_id IN (SELECT manager_id FROM users1 WHERE unique_id ='".$id."')")->result();
          $this->session->unset_userdata('message_list');
         $data['message_list'] =$query3 ;
         $this->session->set_userdata('message_list',$data);


         }
         else{
          $query3 =$this->db->query("SELECT unique_id,name FROM `users1` WHERE unique_id IN (SELECT team_leader_id FROM users1 WHERE unique_id ='".$id."') OR (team_leader_id IN (SELECT team_leader_id FROM users1 WHERE unique_id='".$id."' ) AND unique_id !='".$id."') OR unique_id IN (SELECT manager_id FROM users1 where unique_id IN (SELECT team_leader_id FROM users1 WHERE unique_id='".$id."' ))")->result();
          $this->session->unset_userdata('message_list');
         $data['message_list'] =$query3 ;
         $this->session->set_userdata('message_list',$data);


         }
      }



 }


function getWorkList(){
  if ($this->session->userdata('user_position')=='Employee') {
      $id= $this->session->userdata('user_id');
   $query = $this->db->query("SELECT work_title,days,work_id,assign_date FROM `work_assign` WHERE sender_id!='".$id."' AND receiver_id='".$id."' AND work_status IN ('submitted','assigned') ORDER BY work_id")->result();

  

  }
  else {
    if ($this->session->userdata('user_position')=='Admin') {

      $id= $this->session->userdata('user_id');
   $query = $this->db->query("SELECT * FROM `admin_work` WHERE work_status IN ('assigned') ORDER BY id")->result();


    }
      else{
   $id= $this->session->userdata('user_id');
   $query = $this->db->query("SELECT work_title,days,descr,work_id,assign_date FROM `work_assign` WHERE receiver_id='".$id."' AND work_status IN ('submitted','assigned') ORDER BY work_id")->result();
 }
}
   $this->session->unset_userdata('work_list');
   $data['work_list'] = $query ;
   $this->session->set_userdata('work_list', $data );



  $query5 = $this->db->query("SELECT * FROM `projects`  ORDER BY project_id")->result();
  $this->session->unset_userdata('project_list');
  $data['project_list'] =$query5 ;
  $this->session->set_userdata('project_list',$data);




  $query6 = $this->db->query("SELECT projects.project_id,projects.assign_manager_id,work_assign.receiver_id,work_assign.work_title FROM projects JOIN work_assign WHERE projects.project_id=work_assign.project_id AND projects.assign_manager_id=work_assign.sender_id")->result();
  $this->session->unset_userdata('recepient_list');
  $data['recepient_list'] =$query6 ;
  $this->session->set_userdata('recepient_list',$data);

 }


function getTeamList(){
$id= $this->session->userdata('user_id');
 $user_position= $this->session->userdata('user_position');
  if ($user_position =='Manager') {
        $query3 =$this->db->query("SELECT unique_id, name ,position FROM `users1` WHERE  manager_id='".$id."'")->result();
          $this->session->unset_userdata('team_list');
       $data['team_list'] =$query3 ;
      $this->session->set_userdata('team_list',$data);


      $query4 =$this->db->query("SELECT users1.name , work_assign.work_title,work_assign.assign_date,work_assign.work_id, work_assign.days FROM `work_assign`,`users1` WHERE work_assign.receiver_id=users1.unique_id AND work_assign.work_status IN ('submitted','assigned')  AND (work_assign.sender_id='".$id."' AND work_assign.receiver_id IN (SELECT unique_id FROM  `users1` WHERE manager_id='".$id."'))")->result();
        $this->session->unset_userdata('track_list');
      $data['track_list'] =$query4 ;
      $this->session->set_userdata('track_list',$data);

      $query5=$this->db->query("SELECT base_work_id,work_id,days,work_status FROM work_assign WHERE sender_id IN (SELECT unique_id FROM `users1` WHERE manager_id='".$id."')")->result();
        $this->session->unset_userdata('progress');
      $data['progress'] =$query5 ;
      $this->session->set_userdata('progress',$data);

    }
  else{
     if ($user_position =='TeamLeader') {
        $query3 =$this->db->query("SELECT unique_id, name,position FROM `users1` WHERE team_leader_id='".$id."'")->result();
          $this->session->unset_userdata('team_list');
         $data['team_list'] =$query3 ;
         $this->session->set_userdata('team_list',$data);



      $query4 =$this->db->query("SELECT users1.name , work_assign.work_title,work_assign.assign_date,work_assign.work_id, work_assign.days FROM `work_assign`,`users1` WHERE work_assign.receiver_id=users1.unique_id  AND work_assign.work_status IN ('submitted','assigned')  AND (work_assign.sender_id='".$id."' AND work_assign.receiver_id IN (SELECT unique_id FROM `users1` WHERE team_leader_id='".$id."'))")->result();
        $this->session->unset_userdata('track_list');
      $data['track_list'] =$query4 ;
      $this->session->set_userdata('track_list',$data);

      $query5=$this->db->query("SELECT base_work_id,work_id,days,work_status FROM work_assign WHERE sender_id IN (SELECT users1.unique_id FROM `users1` WHERE team_leader_id='".$id."')")->result();
        $this->session->unset_userdata('progress');
      $data['progress'] =$query5 ;
      $this->session->set_userdata('progress',$data);


         }
         else 
          if ($user_position == 'Admin') {
             $query3 =$this->db->query("SELECT unique_id, name,position FROM `users1` WHERE position='Manager'")->result();
          $this->session->unset_userdata('team_list');
         $data['team_list'] =$query3 ;
         $this->session->set_userdata('team_list',$data);

          }
      }
}

function chatting($chat_id,$sender_id){

    $id= $this->session->userdata('user_id');

   $query = $this->db->query("SELECT messages.message_id,messages.receiver_id,message_list.created_by,messages.message_content,messages.create_date FROM `messages` JOIN `message_list` WHERE message_list.chat_id=messages.chat_id AND messages.receiver_id IN ('".$sender_id."','".$id."') AND message_list.created_by IN ('".$id."','".$sender_id."')  ORDER BY messages.create_date ASC ")->result();

$query4 = $this->db->query("SELECT name FROM users1 WHERE unique_id='".$sender_id."'");
   $this->session->set_userdata('receiver_name', $query4->row()->name );


    $query3 = $this->db->query("UPDATE `messages` SET `message_status`='received' WHERE `chat_id`='".$chat_id."' AND `receiver_id`='".$id."'");
   

   if ($query3 < 0) {
   $query2 = $this->db->query("UPDATE `message_list` SET `message_status`='received' WHERE `chat_id`='".$chat_id."'");
    }
    else{
      $query2 = $this->db->query("UPDATE `message_list` SET `message_status`='send' WHERE `chat_id`='".$chat_id."'");
    }

   return $query;
}


 function addToMessageList($message,$receiver_id)
 {
      $id= $this->session->userdata('user_id');
      $query = $this->db->query("SELECT DISTINCT messages.chat_id FROM message_list JOIN messages WHERE message_list.chat_id=messages.chat_id AND (message_list.created_by='".$id."' AND messages.receiver_id='".$receiver_id."')");
      if ($query->num_rows() > 0){
             $row = $query->row();
             $chat_id = $row->chat_id;
           }
        $query3 = $this->db->query("SELECT name FROM users1 WHERE unique_id='".$receiver_id."'");
   $this->session->set_userdata('receiver_name', $query3->row()->name );
        $this->session->set_userdata('chat_id', $chat_id );
         $query1 = array(
           'message_id'=>'NULL',
          'chat_id' => $chat_id,
          'receiver_id' => $receiver_id,
          'message_content' => $message,
          'create_date'=> date('Y-m-d H:i:s'),
          'message_status' => 'send',

      );
      $this->db->insert('messages',$query1);

      $message1 = mb_substr($message, 0, 10);
    $date = date('Y-m-d H:i:s');
   $query2 = $this->db->query("UPDATE `message_list` SET `header`='".$message1."' ,`message_status`='send',`create_date`='".$date."' WHERE `chat_id`='".$chat_id."'");


   return true;

 }


function createNewMessagelink($receiver_id,$message){

      $id= $this->session->userdata('user_id');
      $query = $this->db->query("SELECT DISTINCT messages.chat_id FROM message_list JOIN messages WHERE message_list.chat_id=messages.chat_id AND (message_list.created_by='".$id."' AND messages.receiver_id='".$receiver_id."')");
      if ($query->num_rows() > 0){

        $this->CI_auth->addToMessageList($message,$receiver_id);
           }
           else
           {
      $message1 = mb_substr($message, 0, 10);
         $query = array(
          'chat_id' => 'NULL',
          'created_by' => $this->session->userdata('user_id'),
          'header' => $message1,
          'create_date'=> date('Y-m-d H:i:s'),
          'message_status' => 'send',
      );
   $this->db->insert('message_list',$query);
   $query1 = $this->db->query("SELECT chat_id FROM `message_list` ORDER BY `chat_id` DESC LIMIT 1");
   $this->session->set_userdata('chat_id', $query1->row()->chat_id );

 $query3 = $this->db->query("SELECT name FROM users1 WHERE unique_id='".$receiver_id."'");
   $this->session->set_userdata('receiver_name', $query3->row()->name );
      $query = array(
           'message_id'=>'NULL',
          'chat_id' => $this->session->userdata('chat_id'),
          'receiver_id' => $receiver_id,
          'message_content' => $message,
          'create_date'=> date('Y-m-d H:i:s'),
          'message_status' => 'send',


      );
   $this->db->insert('messages',$query);
   return true ;
 }


 }


 function work($work_id){
  $id= $this->session->userdata('user_id');

  $query=$this->db->query("SELECT work_title , descr ,file_name, work_id ,project_id FROM `work_assign` WHERE work_id='".$work_id."'")->result();
       $data['title'] =$query ;
       $this->session->set_userdata('title', $data );


  $query1=$this->db->query("SELECT * FROM `work_assign` WHERE base_work_id='".$work_id."' AND sender_id='".$id."' AND work_status IN ('submitted','assigned')")->result();
       $data['sub_work'] =$query1 ;
        $this->session->set_userdata('sub_work', $data );

 }




 function assignProjectWork($project_id,$base_work_id,$receiver,$title,$descr,$days){  
 $query = array(
           'project_id'=>$project_id,
          'work_id' => 'NULL',
          'base_work_id'=>$base_work_id,
          'sender_id'=>$this->session->userdata('user_id'),
          'receiver_id' => $receiver,
          'work_title' => $title,
          'days'=>$days,
          'descr'=>$descr,
          'assign_date'=>date('Y-m-d'),
      );
   $this->db->insert('work_assign',$query);    
   if ($this->db->affected_rows() > 0) {
         return true;
         }
         else {
         return false;
         }
  }



 function addNewWork($title,$descr,$days,$file_name){  
 $query = array(
           'project_id'=>'NULL',
          'work_id' => 'NULL',
          'base_work_id'=>'NULL',
          'sender_id'=>'NULL',
          'receiver_id' => $this->session->userdata('user_id'),
          'work_title' => $title,
          'descr'=>$descr,
          'days'=>$days,
          'file_name'=>$file_name,
          'assign_date'=>date('Y-m-d'),
      );
   $this->db->insert('work_assign',$query);    
   if ($this->db->affected_rows() > 0) {
         return true;
         }
         else {
         return false;
         }
  }


 function addNewAdminWork($title){  
 $query = array(
           'id'=>'NULL',
          'work_title' => $title,
          'assign_date'=>date('Y-m-d'),
          'work_status'=>'assigned',
      );
   $this->db->insert('admin_work',$query);    
   if ($this->db->affected_rows() > 0) {
         return true;
         }
         else {
         return false;
         }
  }




 function assignNewWork($title,$descr,$days,$receiver,$file_name){  
 $query = array(
           'project_id'=>'NULL',
          'work_id' => 'NULL',
          'base_work_id'=>'NULL',
          'sender_id'=>$this->session->userdata('user_id'),
          'receiver_id' => $receiver,
          'work_title' => $title,
          'descr'=>$descr,
          'days'=>$days,
          'assign_date'=>date('Y-m-d'),
          'file_name'=>$file_name,
      );
   $this->db->insert('work_assign',$query);    
   if ($this->db->affected_rows() > 0) {
         return true;
         }
         else {
         return false;
         }
  }



 function assignNewProject($title,$descr,$days,$receiver,$file_name,$type){  

 $query = array(
           'project_id'=>'NULL',
          'assign_manager_id' => $receiver,
          'project_name' => $title,
          'project_desc' => $descr,
          'project_type' => $type,
          'project_sub_date'  => date("Y-m-d",strtotime("+$days")),
          'project_assign_date' => date('Y-m-d'),
          'planning_date' => date("Y-m-d",strtotime("+1")),
          'designing_date' => date("Y-m-d",strtotime("+($days/3)")),
          'testing_date' => date("Y-m-d",strtotime("+(2*($days/3)")),
          'file_name' => $file_name,
      );
   $this->db->insert('projects',$query);   

   $project = $this->db->query("SELECT project_id FROM `projects` ORDER BY `project_assign_date` DESC LIMIT 1");
   $p_id= $project->row()->project_id ;

   $query1 = array(
           'project_id'=>$p_id,
          'work_id' => 'NULL',
          'base_work_id'=>'NULL',
          'sender_id'=>$this->session->userdata('user_id'),
          'receiver_id' => $receiver,
          'work_title' => $title,
          'descr'=>$descr,
          'days'=>$days,
          'assign_date'=>date('Y-m-d'),
          'file_name'=>$file_name,
      );
   $this->db->insert('work_assign',$query1);    

   if ($this->db->affected_rows() > 0) {
         return true;
         }
         else {
         return false;
         }
  }


  function submitWork($work_id){


        $query = $this->db->query("UPDATE `work_assign` SET `completion_date`='".date('Y-m-d')."' ,`work_status`='submitted' WHERE `work_id`='".$work_id."' ");
        return $query;
  }


  function getEmpInfo($position1){

        if($position1 == "All"){

                  $query = $this->db->query("SELECT * FROM `users1` WHERE position IN ( 'Employee','TeamLeader','Manager') ")->result();
                          return $query;


        }else
        {
        $query = $this->db->query("SELECT * FROM `users1` WHERE position ='".$position1."' ")->result();
                return $query;

      }
  }
function getIdeaInfo($status){

        if($status == "All"){

                  $query = $this->db->query("SELECT * FROM `idea` WHERE status IN ( 'accept','reject','send') ")->result();
                          return $query;


        }else
        {
        $query = $this->db->query("SELECT * FROM `idea` WHERE status ='".$status."' ")->result();
                return $query;

      }
  }

  function submitAdminWork($work_id){

        $query = $this->db->query("UPDATE `admin_work` SET `work_status`='completed' WHERE `id`='".$work_id."' ");
        return $query;
  }


function markAsCompleteWork($work_id){
  if ($this->session->userdata('user_position')=='Employee'){
 $query = $this->db->query("UPDATE `work_assign` SET `work_status`='completed' WHERE `work_id`='".$work_id."'");
        return $query;  }
        else{

        $query = $this->db->query("UPDATE `work_assign` SET `work_status`='completed' WHERE `work_id`='".$work_id."' OR `base_work_id`='".$work_id."'");
        return $query;
      }


  }

function salaryIncrement($unique_id,$salary,$position){
    if ($position == "") {
             $query2=$this->db->query(" UPDATE users1 SET reputation_status='N',salary = salary +'".$salary."',reputation ='0' WHERE unique_id='".$unique_id."'");


        }
        else {
               $query2=$this->db->query(" UPDATE users1 SET reputation_status='N',position='".$position."',salary = salary +'".$salary."'  , reputation ='0' WHERE unique_id='".$unique_id."'");
              }
}

  

  function insertEmployee($input_data){

    $this->db->insert('users1', $input_data);
    if ($this->db->affected_rows() > 0) {
         return true;
         }
         else {
         return false;
         }


  }

function getIdeasGraph(){


  $query = $this->db->query("SELECT COUNT(status) as count1, status FROM idea GROUP BY status")->result();

  return $query;

  
}

function getProjectGraph(){


  $query = $this->db->query("SELECT COUNT(project_type) as count1, project_type FROM projects GROUP BY project_type")->result();

  return $query;

  
}

/*Read the data from DB */
  Public function getEvents()
  {
    
  $sql = "SELECT * FROM events WHERE events.date BETWEEN ? AND ? ORDER BY events.date ASC";
  return $this->db->query($sql, array($_GET['start'], $_GET['end']))->result();

  }

/*Create new events */

  Public function addEvent()
  {

  $sql = "INSERT INTO events (title,events.date, description, color) VALUES (?,?,?,?)";
  $this->db->query($sql, array($_POST['title'], $_POST['date'], $_POST['description'], $_POST['color']));
    return ($this->db->affected_rows()!=1)?false:true;
  }

  /*Update  event */

  Public function updateEvent()
  {

  $sql = "UPDATE events SET title = ?, events.date = ?, description = ?, color = ? WHERE id = ?";
  $this->db->query($sql, array($_POST['title'], $_POST['date'], $_POST['description'], $_POST['color'], $_POST['id']));
    return ($this->db->affected_rows()!=1)?false:true;
  }


  /*Delete event */

  Public function deleteEvent()
  {

  $sql = "DELETE FROM events WHERE id = ?";
  $this->db->query($sql, array($_GET['id']));
    return ($this->db->affected_rows()!=1)?false:true;
  }

  /*Update  event */

  Public function dragUpdateEvent()
  {
      $date=date('Y-m-d h:i:s',strtotime($_POST['date']));

      $sql = "UPDATE events SET  events.date = ? WHERE id = ?";
      $this->db->query($sql, array($date, $_POST['id']));
    return ($this->db->affected_rows()!=1)?false:true;


  }





  
}