* What is this repository for? 

* It is a portal where company will give id and password to its employees. When employee will login to the system he/she can see the  dashboard page. The dashboard page consist of various activities like employee can see what task is assigned by manager to him/her. Employees can add the work that they have to complete in their to- do list, employees can see their profile with all details as well as projects which are in process, employees can share the idea or solution on particular projects.

* Developed the portal as the part of Final year project with the team of 3 members. The project is developed using HTML, CSS, JavaScript, PHP language, MySQL database & with Bootstrap technology using Codeigniter framework.


*  How do I get set up? 

* Install XampServer or WampServer
* Copy the code to htdocs folder
* Create a database 'corporate' and import .sql file
* Start the Xampp Control Panel 
* Write the url as "localhost/corporate/"

