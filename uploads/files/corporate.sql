-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2017 at 06:19 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `corporate`
--

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE `captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(152, 1490087568, '::1', 'Hf8GLnXx'),
(153, 1490087636, '::1', '98BFD5ZI');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL COMMENT 'Upload Date',
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Unblock, 0=Block'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `file_name`, `created`, `modified`, `status`) VALUES
(44, '4.27-BE_computer_Engg2.pdf', '2017-01-06 07:14:50', '2017-01-06 07:14:50', 1),
(45, 'adultery-paulo-coelho.pdf', '2017-01-06 08:11:24', '2017-01-06 08:11:24', 1),
(46, 'note.txt', '2017-01-06 08:12:37', '2017-01-06 08:12:37', 1),
(49, 'poems.txt', '2017-01-06 18:02:09', '2017-01-06 18:02:09', 1),
(50, 'DWM_1(me).docx', '2017-03-19 03:44:23', '2017-03-19 03:44:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `idea`
--

CREATE TABLE `idea` (
  `idea_id` int(100) NOT NULL,
  `project_id` int(11) NOT NULL,
  `to_whom` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `idea_date` date DEFAULT NULL,
  `result_date` date DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `ID` int(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `idea`
--

INSERT INTO `idea` (`idea_id`, `project_id`, `to_whom`, `subject`, `code`, `idea_date`, `result_date`, `status`, `ID`) VALUES
(38, 1, 'Manager', 'huefbdn', 'ihbkn vf', '2017-01-17', '2017-01-17', 'accept', 5),
(37, 1, 'Manager', 'hbjfhvjw', 'hnbhbjfb m', '2017-01-17', '2017-01-18', 'accept', 5);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `chat_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message_content` varchar(500) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message_status` varchar(10) NOT NULL DEFAULT 'received'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `chat_id`, `receiver_id`, `message_content`, `create_date`, `message_status`) VALUES
(64, 39, 7, 'hiiiiiiiiii', '2016-12-23 10:15:39', 'received'),
(69, 40, 20, 'hiiiiii', '2016-12-23 13:14:21', 'received'),
(84, 4, 9, 'yee dude', '2016-12-25 00:56:40', 'received'),
(65, 1, 9, 'hey u', '2016-12-23 15:49:40', 'received'),
(66, 3, 9, 'okkk', '2016-12-23 15:50:08', 'received'),
(67, 4, 9, 'im d', '2016-12-23 15:50:29', 'received'),
(68, 5, 9, 'ohhhh', '2016-12-23 15:50:50', 'received'),
(80, 5, 9, 'okkk', '2016-12-24 00:48:22', 'received'),
(73, 43, 8, 'ohhh', '2016-12-23 22:23:14', 'received'),
(79, 48, 15, 'heyyyyyyyyy', '2016-12-24 00:01:22', 'received'),
(81, 40, 20, 'no yaar', '2016-12-24 00:52:28', 'received'),
(82, 5, 9, 'okk i will call u dont worry', '2016-12-24 00:53:10', 'received'),
(83, 39, 7, 'ok boss', '2016-12-25 00:53:16', 'received'),
(85, 4, 9, 'what', '2016-12-25 00:57:30', 'received'),
(86, 4, 9, 'yaesss', '2016-12-25 00:57:41', 'received'),
(87, 39, 7, 'no nothing is special dude', '2016-12-25 00:58:25', 'received'),
(88, 40, 20, 'ya bro', '2016-12-28 09:07:57', 'received'),
(89, 40, 20, 'goods to go', '2016-12-28 09:41:32', 'received'),
(90, 5, 9, 'hii', '2016-12-28 09:42:52', 'received'),
(91, 40, 20, 'hii', '2016-12-30 01:40:37', 'received'),
(92, 43, 8, 'hhj', '2016-12-30 02:08:30', 'received'),
(100, 40, 20, 'ja be', '2017-01-28 04:22:10', 'received'),
(101, 40, 20, 'areeeeeeeeee', '2017-01-28 04:23:00', 'received'),
(102, 5, 9, 'hiiiiiiiiiiiiiii', '2017-01-28 05:06:47', 'received'),
(103, 40, 20, 'ja re tu', '2017-01-28 05:09:47', 'received'),
(104, 40, 20, 'ja re tu', '2017-01-28 05:10:21', 'received'),
(105, 40, 20, 'areeeeeeeeeeeee', '2017-01-28 05:11:03', 'received'),
(106, 40, 20, 'hnfew', '2017-01-28 05:16:03', 'received'),
(107, 40, 20, 'uijhbujh', '2017-01-28 05:18:13', 'received'),
(108, 39, 7, 'ja re', '2017-01-28 05:20:03', 'received'),
(109, 5, 9, 'hiiiiiiiiiii', '2017-02-02 06:48:10', 'received'),
(110, 40, 20, 'noooooooooooo', '2017-02-02 07:24:00', 'received'),
(111, 5, 9, 'abe ja be', '2017-02-02 07:25:11', 'received'),
(112, 43, 8, 'hiii', '2017-02-08 11:46:36', 'received'),
(113, 43, 8, 'no yaar', '2017-02-08 12:24:37', 'received'),
(115, 50, 5, 'oh\r\n', '2017-02-08 12:55:36', 'received'),
(116, 51, 21, 'bn ', '2017-02-08 13:08:30', 'received'),
(117, 40, 20, 'yess', '2017-03-17 00:52:10', 'received'),
(118, 5, 9, 'yes i can doo', '2017-03-17 00:55:45', 'received'),
(119, 40, 20, 'no i can not', '2017-03-17 00:57:42', 'received'),
(120, 40, 20, 'but we have proper knowledge', '2017-03-17 00:58:12', 'received'),
(121, 39, 7, 'dkjsfkf', '2017-03-24 04:36:46', 'received'),
(122, 4, 9, 'hhiiii rushabh how are you??/', '2017-03-24 04:41:22', 'received');

-- --------------------------------------------------------

--
-- Table structure for table `message_list`
--

CREATE TABLE `message_list` (
  `chat_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `header` varchar(100) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message_status` varchar(10) NOT NULL DEFAULT 'received'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_list`
--

INSERT INTO `message_list` (`chat_id`, `created_by`, `header`, `create_date`, `message_status`) VALUES
(1, 8, 'hii', '2016-12-19 11:46:13', 'received'),
(2, 9, 'ki', '2016-12-19 11:46:29', 'received'),
(3, 15, 'good by', '2016-12-20 08:55:07', 'received'),
(5, 20, 'yes i can ', '2017-03-17 00:55:45', 'received'),
(39, 9, 'dkjsfkf', '2017-03-24 04:36:47', 'received'),
(4, 7, 'hhiiii rus', '2017-03-24 04:41:22', 'received'),
(40, 9, 'but we hav', '2017-03-17 00:58:12', 'received'),
(43, 9, 'no yaar', '2017-02-08 12:24:37', 'received'),
(50, 8, 'oh\r\n', '2017-02-08 12:55:36', 'received'),
(51, 8, 'bn ', '2017-02-08 13:08:30', 'received');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(10) NOT NULL,
  `project_name` varchar(50) NOT NULL,
  `project_type` varchar(30) DEFAULT NULL,
  `project_desc` varchar(1000) NOT NULL,
  `project_assign_date` timestamp NOT NULL,
  `project_sub_date` timestamp NOT NULL,
  `assign_manager_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `project_name`, `project_type`, `project_desc`, `project_assign_date`, `project_sub_date`, `assign_manager_id`) VALUES
(1, 'Web Portal for organisation', 'Current Project', 'My Organization is a page in the portal website that contains a list of your organization\'s members and links to find content and groups that belong to your organization. All members of the organization can view this page to see their own profile and information about other members, as well as access links to find the organization\'s content and groups.\n\nAdministrators have access to additional functionality through the My Organization page, including options to add members, set roles, customize the website, and manage the organization\'s content and groups. See the Administrator Guide for more information.', '2016-12-29 05:20:14', '2016-12-29 05:20:14', 9),
(2, 'Data Analysis of Sales & Marketing', 'Future Project', 'Take help of data warehouses & Analyse the data of marketing & sales department....\nso we can do some changes into perticular department for future profit..', '2017-01-05 17:55:03', '2017-01-05 17:55:03', 9);

-- --------------------------------------------------------

--
-- Table structure for table `users1`
--

CREATE TABLE `users1` (
  `unique_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(225) CHARACTER SET utf8 NOT NULL,
  `username` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `position` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `salt` varchar(50) CHARACTER SET utf8 NOT NULL,
  `phone` int(11) NOT NULL,
  `date_of_birth` date NOT NULL,
  `reputation` int(11) NOT NULL,
  `reputation_status` varchar(1) NOT NULL DEFAULT 'N',
  `hired_date` date DEFAULT NULL,
  `photo` varchar(50) NOT NULL,
  `salary` int(11) NOT NULL,
  `team_leader_id` int(11) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users1`
--

INSERT INTO `users1` (`unique_id`, `name`, `email`, `username`, `password`, `position`, `address`, `gender`, `salt`, `phone`, `date_of_birth`, `reputation`, `reputation_status`, `hired_date`, `photo`, `salary`, `team_leader_id`, `manager_id`) VALUES
(8, 'Ganesh Zore', 'ganeshzore666@gmail.com', 'ganeshzore', '45090babd35dbec24ff8284ad1b7e0d22d904f73', 'Employee', 'bandra', 'Male', 'mEZyHt,b', 2147483647, '1995-09-28', 5, 'N', NULL, 'assets/images/avatar4.png', 10000, 21, NULL),
(9, 'Rushabh Wadkar', 'ru@gmail.com', 'rushabh', '115384e65d22f8654bdaad1430bc875965626141', 'Manager', 'Panvel', 'Male', './dCR4h?', 56565346, '2017-01-25', 9, 'N', NULL, 'assets/images/avatar.png', 1000000, NULL, NULL),
(7, 'Swapnil Velunde', 'gan@gmil.com', 'ganeshz', '28624ecc355a2676ccbd272ec9c9cf2f43e003ca', 'Employee', 'worli', 'Male', 'fk!z8UKH', 5676475, '2017-01-10', 15, 'N', NULL, 'assets/images/avatar.png', 35000, 20, NULL),
(24, 'Sharvil', 'sharvil@gmail.com', 'sharvil', '02cdee1c3dda1bcbaa57c1f20a894134f32af413', 'TeamLeader', 'hjdsj', 'Female', 'X98K/bem', 2147483647, '2017-03-23', 0, 'N', NULL, '', 0, NULL, NULL),
(5, 'Dinesh Zore', 'ganeshzore666@gmail.com', 'dinesh', 'eee6fcb79060c7080e384abb1d7ca3dbe8e33b1f', 'Employee', 'worli,mumbai-400199', 'Male', '/T!!ecdg', 2147483647, '1983-12-31', 15, 'N', NULL, 'assets/images/avatar4.png', 36000, 21, NULL),
(100, 'admin', 'admin@gmail.com', 'admin', '2718ab40544164201dc11f15ad3e1c7f6a53dc69', 'Admin', 'office', 'Male', 'v,8JaVyC', 2147483647, '2017-03-16', 0, 'N', NULL, '', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `work_assign`
--

CREATE TABLE `work_assign` (
  `project_id` int(11) DEFAULT NULL,
  `work_id` int(11) NOT NULL,
  `base_work_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) NOT NULL,
  `work_title` varchar(100) NOT NULL,
  `assign_date` date DEFAULT NULL,
  `days` int(11) NOT NULL,
  `work_status` varchar(20) NOT NULL DEFAULT 'assigned',
  `completion_date` date DEFAULT NULL,
  `descr` varchar(500) NOT NULL,
  `file_name` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_assign`
--

INSERT INTO `work_assign` (`project_id`, `work_id`, `base_work_id`, `sender_id`, `receiver_id`, `work_title`, `assign_date`, `days`, `work_status`, `completion_date`, `descr`, `file_name`) VALUES
(1, 1, 1, 1, 9, 'Web Portal for organistaion', '2016-12-22', 30, 'assigned', NULL, 'My Organization is a page in the portal website that contains a list of your organization\'s members and links to find content and groups that belong to your organization. All members of the organization can view this page to see their own profile and information about other members, as well as access links to find the organization\'s content and groups.  Administrators have access to additional functionality through the My Organization page, including options to add members, set roles', ''),
(2, 3, 3, 1, 9, 'Data Analysis of Sales & Marketing', '2017-01-05', 50, 'assigned', NULL, 'Take help of data warehouses & Analyse the data of marketing & sales department....\nso we can do some changes into perticular department for future profit..', NULL),
(0, 7, 0, 0, 9, 'Advertising Of Project', '2016-12-26', 8, 'assigned', NULL, 'Take help of regular Customer And Start the advertisement of new product...\r\n', NULL),
(0, 8, 7, 9, 20, 'Survey', '2016-12-28', 9, 'assigned', NULL, 'Do the Survey on Customer Requirement...', NULL),
(0, 33, 8, 20, 7, 'survey final stage', '2016-12-31', 6, 'completed', NULL, 'do it', NULL),
(1, 34, 10, 21, 8, 'abc', '2017-01-12', 12, 'completed', NULL, 'abc', NULL),
(2, 23, 3, 9, 20, 'Analyse the data of sales', '2017-01-07', 20, 'completed', '2017-01-12', 'take help of sales department...', NULL),
(1, 10, 1, 9, 21, 'Study On Existing System', '2017-01-01', 3, 'completed', NULL, 'Understand the existing system to make Web portal............', NULL),
(1, 11, 1, 9, 21, 'Design Web Portal', '2017-01-05', 5, 'assigned', NULL, 'well design structure is has to design for proper integration ...', NULL),
(2, 30, 3, 9, 20, 'abcd', '2017-01-11', 12, 'submitted', '2017-03-17', 'abcd', NULL),
(0, 31, 8, 20, 7, 'do survey on company ', '2016-12-29', 5, 'completed', NULL, 'do it man', NULL),
(1, 35, 10, 21, 8, 'gfd', '2017-01-12', 5, 'completed', NULL, 'gfd', NULL),
(1, 36, 10, 21, 5, 'ytuhj', '2017-01-12', 20, 'completed', '2017-01-12', 'bn', NULL),
(1, 37, 10, 21, 5, 'iej', '2017-01-12', 30, 'completed', '2017-01-12', 'njk', NULL),
(0, 38, 7, 9, 21, 'few', '2017-01-12', 23, 'assigned', NULL, 'gregw', NULL),
(0, 39, 0, 0, 9, 'hjghb', '2017-01-12', 43, 'assigned', NULL, 'tf', NULL),
(0, 40, 39, 9, 21, 'grfe', '2017-01-12', 12, 'completed', NULL, 'vre', NULL),
(0, 41, 40, 21, 5, 'hfcfg', '2017-01-12', 23, 'completed', NULL, 'gfcv', NULL),
(0, 42, 33, 7, 7, 'def', '2017-01-12', 12, 'completed', NULL, 'fsv', NULL),
(1, 43, 10, 21, 5, 'hds', '2017-01-15', 22, 'completed', NULL, 'b nhn', NULL),
(1, 44, 43, 5, 5, '5grf', '2017-01-15', 22, 'completed', NULL, 'efbe', NULL),
(1, 45, 43, 5, 5, 'fe4cdw', '2017-01-15', 11, 'assigned', NULL, 'efc', NULL),
(0, 46, 8, 20, 7, 'vjnkj', '2017-02-02', 34, 'completed', '2017-02-02', 'jwk m', NULL),
(0, 47, 8, 20, 7, 'hjbn ', '2017-02-02', 3, 'submitted', '2017-02-02', 'YRTFGCVYJ', NULL),
(2, 48, 30, 20, 7, 'fbc', '2017-02-02', 23, 'assigned', NULL, 'gre', NULL),
(2, 49, 30, 20, 7, 'efwd', '2017-02-02', 32, 'assigned', NULL, 'greefqdav', NULL),
(0, 50, 47, 7, 7, 'hjedn', '2017-02-02', 1, 'completed', NULL, 'yuhjfvn', NULL),
(2, 51, 48, 7, 7, 'jgh', '2017-02-02', 2, 'completed', NULL, 'jhghjb', NULL),
(0, 52, 0, 9, 21, 'go to meeting', '2017-03-19', 1, 'assigned', NULL, 'with the boss', NULL),
(1, 53, 1, 9, 20, 'ksjd', '2017-03-19', 4, 'completed', NULL, 'njsdk', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `idea`
--
ALTER TABLE `idea`
  ADD PRIMARY KEY (`idea_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `message_list`
--
ALTER TABLE `message_list`
  ADD PRIMARY KEY (`chat_id`),
  ADD UNIQUE KEY `chat_id` (`chat_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `users1`
--
ALTER TABLE `users1`
  ADD PRIMARY KEY (`unique_id`);

--
-- Indexes for table `work_assign`
--
ALTER TABLE `work_assign`
  ADD PRIMARY KEY (`work_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `idea`
--
ALTER TABLE `idea`
  MODIFY `idea_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `message_list`
--
ALTER TABLE `message_list`
  MODIFY `chat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `users1`
--
ALTER TABLE `users1`
  MODIFY `unique_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `work_assign`
--
ALTER TABLE `work_assign`
  MODIFY `work_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
