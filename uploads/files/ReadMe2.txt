Author : CodexWorld
Author URI : http://www.codexworld.com/
Author Email: contact@codexworld.com
Tutorial Link: http://www.codexworld.com/codeigniter-upload-multiple-files-images/

Installation Instructtion:
==================================================
1. Import the files.sql file into the database of your CodeIgniter application.
2. Move all files to the same directory of your CodeIgniter application
3. Move the uploads/ directory to the project's root folder.
4. Open the URL (http://localhost/project_folder_name/upload_files) on the browser and upload multiple files.

============ May I Help You ===========
If you have any query about this script, please feel free to comment here - http://www.codexworld.com/codeigniter-upload-multiple-files-images/#respond. We will reply your query ASAP.